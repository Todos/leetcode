package main

// Find the largest palindrome made from the product of two n-digit numbers.
//
// Since the result could be very large, you should return the largest palindrome mod 1337.
//
// Example:
//
// Input: 2
//
// Output: 987
//
// Explanation: 99 x 91 = 9009, 9009 % 1337 = 987
//
// Note:
//
// The range of n is [1,8].

import (
	"fmt"
	"strconv"
)

func reverse(s string) (result string) {
	for _, v := range s {
		result = string(v) + result
	}
	return
}

func isPalindrome(n int64) bool {
	s := strconv.FormatInt(n, 10)
	return s == reverse(s)
}

func palindromesBruteForce(n1 int, n1_min int, n2 int, n2_min int) (result [][]int64) {
	for i := n1; i > n1_min; i = i - 1 {
		for j := n2; j > n2_min; j = j - 1 {
			product := int64(i) * int64(j)
			if isPalindrome(product) {
				res := []int64{int64(i), int64(j), product}
				result = append(result, res)
				if len(result) >= 100 {
					return
				}
			}
		}
	}
	return
}

func largestPalindromeBruteForce(n1 int, n1_min int, n2 int, n2_min int) (max int64, ci int64, cj int64) {
	for _, v := range palindromesBruteForce(n1, n1_min, n2, n2_min) {
		if v[2] > max {
			ci, cj, max = v[0], v[1], v[2]
		}
	}
	return
}

func largestPalindrome(n int) int {
	lookup := map[int]int{
		1: 9,
		2: 99,
		3: 999,
		4: 9999,
		5: 99999,
		6: 999999,
		7: 9999999,
		8: 99999999}

	num := lookup[n]
	max, _, _ := largestPalindromeBruteForce(num, 0, num, 0)
	// fmt.Printf("%v => %v %v %v\n", num, max, i, j)
	return int(max % 1337)
}

func main() {
	// fmt.Println(largestPalindrome(7))
	// fmt.Println("---")
	for x := 1; x <= 8; x++ {
		fmt.Println(largestPalindrome(x))
	}
}

// ➜  src git:(master) ✗ time go run main.go
// 9
// 987
// 123
// 597
// 152
// 1218
// 1168
// 475
// go run main.go  7,48s user 0,12s system 103% cpu 7,362 total

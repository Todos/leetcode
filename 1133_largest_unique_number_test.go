package main

import (
    "testing"
)

var data = []struct {
    nums1 []int
    res int
}{
    {[]int{5, 7, 3, 9, 4, 9, 8, 3, 1}, 8},
    {[]int{9, 9, 8, 8}, -1},
}

func Test1(t *testing.T) {
    for _, d := range data {
        res := largestUniqueNumber(d.nums1)
        if res != d.res {
            t.Error("For", d.nums1, "got", res, "expected", d.res)
        }
    }
}

package main

/*
Write a function to find the longest common prefix string amongst an array of strings.
If there is no common prefix, return an empty string "".

Example 1:

Input: strs = ["flower","flow","flight"]
Output: "fl"
Example 2:

Input: strs = ["dog","racecar","car"]
Output: ""
Explanation: There is no common prefix among the input strings.

Constraints:

0 <= strs.length <= 200
0 <= strs[i].length <= 200
strs[i] consists of only lower-case English letters.
 */

func longestCommonPrefix(strs []string) string {
	if len(strs) == 0 {
		return ""
	}

	var matches []byte
	for i := 0; i < 200; i++ {
		if i >= len(strs[0]) {
			continue
		}

		var currentByte = strs[0][i]
		var isAllMatch = true

		for j := 0; j < len(strs); j++ {
			if i >= len(strs[j]) {
				isAllMatch = false
				break
			}

			if strs[j][i] != currentByte {
				isAllMatch = false
				break
			}
		}

		if !isAllMatch {
			break
		}

		matches = append(matches, currentByte)
	}

	if len(matches) == 0 {
		return ""
	}

	return string(matches)
}

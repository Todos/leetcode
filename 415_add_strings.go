package main

import (
    "fmt"
    "strconv"
    "strings"
)

/*
Given two non-negative integers num1 and num2 represented as string, return the sum of num1 and num2.

Note:

The length of both num1 and num2 is < 5100.
Both num1 and num2 contains only digits 0-9.
Both num1 and num2 does not contain any leading zero.
You must not use any built-in BigInteger library or convert the inputs to integer directly.
 */

func addStrings(num1 string, num2 string) string {
    j1 := len(num1)
    j2 := len(num2)
    carry := 0
    d1 := 0
    d2 := 0

    result := []string{}

    for {

        if j1 < 1 {
            d1 = 0
        } else {
            d1, _ = strconv.Atoi(string(num1[j1 - 1]))
        }

        if j2 < 1 {
            d2 = 0
        } else {
            d2, _ = strconv.Atoi(string(num2[j2 - 1]))
        }

        sum := d1 + d2 + carry
        sumStr := strconv.Itoa(sum)
        if sum > 9 {
            carry = 1
            result = append(result, string(sumStr[1]))
        } else {
            carry = 0
            result = append(result, sumStr)
        }

        j1 -= 1
        j2 -= 1

        if j1 < 0 && j2 < 0 {
            break
        }
    }

    result = reverse(result)
    if result[0] == "0" {
        result = result[1:]
    }
    return strings.Join(result, "")
}

func reverse(in []string) []string {
    result := []string{}
    for i := len(in) - 1; i >= 0; i-- {
        result = append(result, in[i])
    }
    return result
}

func main() {
    fmt.Println(addStrings("12", "4"))
    fmt.Println(addStrings("12", "14"))
    fmt.Println(addStrings("", "14"))
    fmt.Println(addStrings("12", ""))
    fmt.Println(addStrings("", ""))
    fmt.Println(addStrings("99", "1"))
    fmt.Println(addStrings("999", "11"))
    fmt.Println(addStrings("0", "0"))
    fmt.Println(reverse([]string{"1", "2"}))
}

package main

import (
    "fmt"
)

/*
Given a string, find the first non-repeating character in it and return it's index. If it doesn't exist, return -1.

Examples:

s = "leetcode"
return 0.

s = "loveleetcode",
return 2.
Note: You may assume the string contain only lowercase letters.
 */

func firstUniqChar(s string) int {
    h1 := map[int32]int{}
    for _, e := range s {
        el := int32(e)
        _, ok := h1[el]
        if ok {
            h1[el] += 1
        } else {
            h1[el] = 1
        }
    }

    for i, e := range s {
        cnt := h1[int32(e)]
        if cnt == 1 {
            return i
        }
    }

    return -1
}

func main() {
    fmt.Println(firstUniqChar("leetcode"), 0)
    fmt.Println(firstUniqChar("loveleetcode"), 2)
    fmt.Println(firstUniqChar(""), -1)
    fmt.Println(firstUniqChar("aaaaa"), -1)
    fmt.Println(firstUniqChar("aaaaabbc"), 7)
}

package main

import (
	"sort"
	"strconv"
	"strings"
)

/*
Given a list of non-negative integers nums, arrange them such that they form the largest number.
Note: The result may be very large, so you need to return a string instead of an integer.

Example 1:
Input: nums = [10,2]
Output: "210"

Example 2:
Input: nums = [3,30,34,5,9]
Output: "9534330"

Example 3:
Input: nums = [1]
Output: "1"

Example 4:
Input: nums = [10]
Output: "10"

Constraints:

1 <= nums.length <= 100
0 <= nums[i] <= 10**9
 */

func largestNumber(nums []int) string {
	numsStr := make([]string, 0)
	for _, i := range nums {
		numsStr = append(numsStr, strconv.Itoa(i))
	}

	sort.Slice(numsStr, func (i int, j int) bool { return numsStr[i]+numsStr[j] > numsStr[j]+numsStr[i] })
	result := strings.Join(numsStr, "")
	if result[0:1] == "0" {
		return "0"
	}
	return result
}
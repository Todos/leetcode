package main

import (
	"fmt"
	"strconv"
	"strings"
)

/*
Given an encoded string, return its decoded string.

The encoding rule is: k[encoded_string], where the encoded_string inside the square brackets is being repeated exactly k times. Note that k is guaranteed to be a positive integer.

You may assume that the input string is always valid; No extra white spaces, square brackets are well-formed, etc.

Furthermore, you may assume that the original data does not contain any digits and that digits are only for those repeat numbers, k. For example, there won't be input like 3a or 2[4].



Example 1:

Input: s = "3[a]2[bc]"
Output: "aaabcbc"
Example 2:

Input: s = "3[a2[c]]"
Output: "accaccacc"
Example 3:

Input: s = "2[abc]3[cd]ef"
Output: "abcabccdcdcdef"
Example 4:

Input: s = "abc3[cd]xyz"
Output: "abccdcdcdxyz"
 */

func decodeString(s string) string {
	return decode(s)
}


type element struct {
	data interface{}
	next *element
}

type stack struct {
	head *element
	Size int
}

func (s *stack) Push(data interface{}) {
	element := new(element)
	element.data = data
	temp := s.head
	element.next = temp
	s.head = element
	s.Size++
}

func (s *stack) Pop() interface{} {
	if s.head == nil {
		return nil
	}
	r := s.head.data
	s.head = s.head.next
	s.Size--

	return r
}

func NewStack() *stack {
	this := new(stack)
	return this
}

func isOpeningBracket(element string) bool {
	return strings.Compare(element, "[") == 0
}

func isClosingBracket(element string) bool {
	return strings.Compare(element, "]") == 0
}

type bracket struct {
	value string
	index int
}

func decode(s string) string {
	stack := NewStack()

	for {
		noBrackets := true
		for index, element := range s {
			element := string(element)
			if isOpeningBracket(element) {
				noBrackets = false
				stack.Push(bracket{value: element, index: index})
			} else if isClosingBracket(element) {
				b := stack.Pop().(bracket)
				encodedString := s[b.index+1:index]
				kValue, startIndex := extractK(s, b.index-1)
				s = fmt.Sprintf("%s%s%s", s[0:startIndex], buildString(encodedString, kValue), s[index+1:])
				break
			}
		}

		if noBrackets {
			break
		}
	}

	return s
}

func extractK(s string, index int) (int, int) {
	startIndex := 0
	for i := index; i >= 0; i-- {
		_, err := strconv.Atoi(string(s[i]))
		if err != nil {
			startIndex = i+1
			break
		}
	}

	result, err := strconv.Atoi(s[startIndex:index+1])
	if err != nil {
		return -1, startIndex
	}
	return result, startIndex
}

func buildString(s string, k int) string {
	result := ""
	for i := 0; i < k; i++ {
		result = fmt.Sprintf("%s%s", result, s)
	}
	return result
}

func main() {
	fmt.Println(decode("3[a2[c]]"))
	fmt.Println(decode("3[a]2[bc]"))
	fmt.Println(decode("2[abc]3[cd]ef"))
	fmt.Println(decode("abc3[cd]xyz"))
}
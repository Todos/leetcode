package main

// Given a string containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.

// An input string is valid if:
//
//     Open brackets must be closed by the same type of brackets.
//     Open brackets must be closed in the correct order.
//
// Note that an empty string is also considered valid.

import (
	"fmt"
	"strings"
)

type element struct {
	data interface{}
	next *element
}

type stack struct {
	head *element
	Size int
}

func (this *stack) Push(data interface{}) {
	element := new(element)
	element.data = data
	temp := this.head
	element.next = temp
	this.head = element
	this.Size++
}

func (this *stack) Pop() interface{} {
	if this.head == nil {
		return nil
	}
	r := this.head.data
	this.head = this.head.next
	this.Size--

	return r
}

func Constructor() *stack {
	this := new(stack)
	return this
}

var brackets = map[string]string{
	"}": "{",
	"]": "[",
	")": "(",
}

func isOpeningBracket(element string) bool {
	return strings.Compare(element, "[") == 0 || strings.Compare(element, "{") == 0 || strings.Compare(element, "(") == 0
}

func isClosingBracket(element string) bool {
	return strings.Compare(element, "]") == 0 || strings.Compare(element, "}") == 0 || strings.Compare(element, ")") == 0
}

func isValid(s string) bool {
	stack := Constructor()
	for _, element := range s {
		element := string(element)
		if isOpeningBracket(element) {
			stack.Push(element)
		} else if isClosingBracket(element) {
			value := stack.Pop()
			if value != brackets[element] {
				return false
			}
		}
	}
	if stack.Size > 0 {
		return false
	}
	return true
}

func main() {
	// For debug
	fmt.Println(true, isValid(""))
	fmt.Println(true, isValid("()"))
	fmt.Println(true, isValid("({})"))
	fmt.Println(true, isValid("[]({})"))
	fmt.Println(false, isValid("(({)"))
	fmt.Println(false, isValid("[]({})["))
	fmt.Println(false, isValid("(("))
	fmt.Println(false, isValid("("))
	fmt.Println(false, isValid("}"))
}

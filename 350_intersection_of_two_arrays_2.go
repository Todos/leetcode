package main

import (
    "fmt"
    "sort"
)

/*
Given two arrays, write a function to compute their intersection.

Example 1:

Input: nums1 = [1,2,2,1], nums2 = [2,2]
Output: [2,2]

Example 2:

Input: nums1 = [4,9,5], nums2 = [9,4,9,8,4]
Output: [4,9]

Note:

    Each element in the result should appear as many times as it shows in both arrays.
    The result can be in any order.

Follow up:

    What if the given array is already sorted? How would you optimize your algorithm?
    What if nums1's size is small compared to nums2's size? Which algorithm is better?
    What if elements of nums2 are stored on disk, and the memory is limited such that you cannot load all elements into the memory at once?
*/

// Naive version, complexity O^2
func intersect(nums1 []int, nums2 []int) []int {
    var (
        i1, i2 int
        len1 = len(nums1)
        len2 = len(nums2)
        result = make([]int, 0)
    )

    sort.Ints(nums1)
    sort.Ints(nums2)

    for i1 < len1 && i2 < len2 {
        if nums1[i1] > nums2[i2] {
            i2++
        } else {
            if nums2[i2] > nums1[i1] {
                i1++
            } else {
                // when both are equal
                result = append(result, nums1[i1])
                i1++
                i2++
            }
        }
    }

    return result
}

func main() {
    fmt.Println(intersect([]int{1, 2}, []int{1, 1}))
    fmt.Println(intersect([]int{1}, []int{1}))
}


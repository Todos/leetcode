package main

import (
	"container/heap"
	"sort"
)

/*

Link: https://leetcode.com/problems/meeting-rooms-ii/

Given an array of meeting time intervals "intervals" where intervals[i] = [starti, endi], return the minimum number of
conference rooms required.

Example 1:

Input: intervals = [[0,30],[5,10],[15,20]]
Output: 2

Example 2:

Input: intervals = [[7,10],[2,4]]
Output: 1
 */

// An IntHeap is a min-heap of ints.
type IntHeap []int

func (h IntHeap) Len() int           { return len(h) }
func (h IntHeap) Less(i, j int) bool { return h[i] < h[j] }
func (h IntHeap) Swap(i, j int)      { h[i], h[j] = h[j], h[i] }

func (h *IntHeap) Push(x interface{}) {
	// Push and Pop use pointer receivers because they modify the slice's length,
	// not just its contents.
	*h = append(*h, x.(int))
}

func (h *IntHeap) Pop() interface{} {
	old := *h
	n := len(old)
	x := old[n-1]
	*h = old[0 : n-1]
	return x
}

func minMeetingRooms(intervals [][]int) int {
	if len(intervals) == 0 {
		return 0
	}

	meetRooms := &IntHeap{}
	heap.Init(meetRooms)

	sort.Slice(intervals, func(i int, j int) bool { return intervals[i][0] < intervals[j][0]})
	heap.Push(meetRooms, intervals[0][1])

	for _, x := range intervals[1:] {
		// Compare start time of the next meeting with the max end time
		if (*meetRooms)[0] <= x[0] {
			// Remove unnecessary room
			heap.Pop(meetRooms)
		}

		// Add a room
		heap.Push(meetRooms, x[1])
	}

	return meetRooms.Len()
}

package main

/*
The Tribonacci sequence Tn is defined as follows:
T0 = 0, T1 = 1, T2 = 1, and Tn+3 = Tn + Tn+1 + Tn+2 for n >= 0.
Given n, return the value of Tn.
*/

func tribonacci(n int) int {
	var state = []int{0, 1, 1}
	return trib(n+1, &state)
}

func trib(n int, state *[]int) int {
	if n <= len(*state) {
		return (*state)[n-1]
	}

	tn := trib(n-1, state) + trib(n-2, state) + trib(n-3, state)
	*state = append(*state, tn)
	return tn
}

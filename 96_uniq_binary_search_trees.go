package main

// Given n, how many structurally unique BST's (binary search trees) that store values 1 ... n?

// Example:
//
// Input: 3
// Output: 5
// Explanation:
// Given n = 3, there are a total of 5 unique BST's:
//
//    1         3     3      2      1
//     \       /     /      / \      \
//      3     2     1      1   3      2
//     /     /       \                 \
//    2     1         2                 3

import (
	"fmt"
)

type Node struct {
	Key   int
	Val   int
	Left  *Node
	Right *Node
}

func traverse(root *Node) {
	if root != nil {
		fmt.Printf("%d ", root.Key)
		traverse(root.Left)
		traverse(root.Right)
	}

}

func constructTrees(start int, end int) []*Node {
	var list []*Node = []*Node{}
	if start > end {
		list = append(list, nil)
		return list
	}

	for i := start; i <= end; i++ {
		var leftTrees []*Node = constructTrees(start, i-1)
		var rightTrees []*Node = constructTrees(i+1, end)

		for j := 0; j < len(leftTrees); j++ {
			var left *Node = leftTrees[j]
			for k := 0; k < len(rightTrees); k++ {
				var right *Node = rightTrees[k]
				var node Node = Node{i, i, nil, nil}
				node.Left = left
				node.Right = right
				list = append(list, &node)

			}
		}
	}
	return list
}

func countTrees(start int, end int) []struct{} {
	var list []struct{}
	if start > end {
		var node struct{}
		list = append(list, node)
		return list
	}

	for i := start; i <= end; i++ {
		leftTrees := countTrees(start, i-1)
		rightTrees := countTrees(i+1, end)

		for j := 0; j < len(leftTrees); j++ {
			for k := 0; k < len(rightTrees); k++ {
				var node struct{}
				list = append(list, node)
			}
		}
	}
	return list
}

func countTreesD(start int, end int) int {
	count := 0
	if start > end {
		return count + 1
	}

	for i := start; i <= end; i++ {
		left := countTreesD(start, i-1)
		right := countTreesD(i+1, end)

		count += (left * right)
	}
	//fmt.Println("1", "s", start, "e", end, "c", count)
	return count
}

func insertNode(root *Node, key int, val int) *Node {
	if root == nil {
		root = &Node{key, val, nil, nil}
	} else if key == root.Key {
		root.Val = val
	} else if key < root.Key {
		// left side we're at
		root.Left = insertNode(root.Left, key, val)
	} else if key > root.Key {
		// right side we're at
		root.Right = insertNode(root.Right, key, val)
	}
	return root
}

func traverseBST(node *Node) {
	if node == nil {
		return
	}
	traverseBST(node.Left)
	fmt.Printf("%d\n", node.Val)
	traverseBST(node.Right)
}

func compareBST(node1 *Node, node2 *Node) bool {
	if node1 == nil && node2 == nil {
		return true
	}
	if node1 == nil || node2 == nil {
		return false
	}
	if node1.Key != node2.Key || node1.Val != node2.Val {
		return false
	}
	return compareBST(node1.Left, node2.Left) && compareBST(node1.Right, node2.Right)
}

func buildBST(list []int) *Node {
	var node *Node = nil
	for _, x := range list {
		node = insertNode(node, x, x)
	}
	return node
}

func numTrees(n int) int {
	//var allTrees []*Node = constructTrees(1, n)
	//return len(allTrees)
	//return len(countTrees(1, n))
	return countTreesD(1, n)
}

func main() {
	// var tmp []int = []int{10, 5, 20, 30, 1}
	// fmt.Println("build:", tmp)
	// node := buildBST(tmp)
	// fmt.Println("traverse:")
	// traverseBST(node)
	// var tmp2 []int = []int{3, 30, 10, 15}
	// node22 := buildBST(tmp2)
	// node23 := buildBST(tmp2)
	// eql := compareBST(node22, node23)
	// fmt.Println("compare:", node22, node23, eql)
	// fmt.Println("traverse:", node23)
	// traverseBST(node23)

	var allTrees []*Node = constructTrees(1, 3)
	for i := 0; i < len(allTrees); i++ {
		// fmt.Printf("%#v\n", allTrees[i])
		traverse(allTrees[i])
		fmt.Printf("\n")
	}

	fmt.Println("---")
	fmt.Println(numTrees(3))
	fmt.Println(numTrees(5))
	fmt.Println(numTrees(0))
	fmt.Println(numTrees(-3))
	//fmt.Println(numTrees(19))

	fmt.Println("---")
	fmt.Println(len(countTrees(1, 3)))
	fmt.Println(len(countTrees(1, 5)))
	fmt.Println(len(countTrees(1, 0)))
	fmt.Println(len(countTrees(1, -3)))
	fmt.Println(len(countTrees(1, 19)))
}

package main

// Design and implement a data structure for Least Recently Used (LRU) cache. It should support the following operations: get and put.
//
// get(key) - Get the value (will always be positive) of the key if the key exists in the cache, otherwise return -1.
// put(key, value) - Set or insert the value if the key is not already present. When the cache reached its capacity, it should invalidate the least recently used item before inserting a new item.
//
// Follow up:
// Could you do both operations in O(1) time complexity?
//
// Example:
//
// LRUCache cache = new LRUCache( 2 /* capacity */ );
//
// cache.put(1, 1);
// cache.put(2, 2);
// cache.get(1);       // returns 1
// cache.put(3, 3);    // evicts key 2
// cache.get(2);       // returns -1 (not found)
// cache.put(4, 4);    // evicts key 1
// cache.get(1);       // returns -1 (not found)
// cache.get(3);       // returns 3
// cache.get(4);       // returns 4

/*
		+----------------+     +----------------+     +----------------+
	nil <---| prev HEAD next |<--->| prev NODE next |<--->| prev TAIL next |---> nil
		+----------------+     +----------------+     +----------------+
*/

import (
	"fmt"
)

type LRUElement struct {
	key   int
	value int
	prev  *LRUElement
	next  *LRUElement
}

type LRUCache struct {
	capacity int
	cache    map[int]*LRUElement
	head     *LRUElement // Need to effectively add
	tail     *LRUElement // Need to effectively remove
}

func Constructor(capacity int) LRUCache {
	return LRUCache{capacity, make(map[int]*LRUElement), nil, nil}
}

func (this *LRUCache) Get(key int) int {
	element, ok := this.cache[key]
	if !ok {
		return -1
	}

	this.removeElement(element)
	this.addElement(element)
	// fmt.Printf("get cache: %#v head %#v tail %#v\n", this.cache, this.head, this.tail)
	return element.value
}

func (this *LRUCache) Put(key int, value int) {
	this.addToCache(key, value)
	// fmt.Printf("put cache: %#v head %#v tail %#v\n", this.cache, this.head, this.tail)
}

func (this *LRUCache) removeElement(element *LRUElement) {
	if element.prev != nil {
		element.prev.next = element.next
	} else {
		this.head = element.next
	}
	if element.next != nil {
		element.next.prev = element.prev
	} else {
		this.tail = element.prev
	}
}

func (this *LRUCache) addElement(element *LRUElement) {
	element.prev = nil
	element.next = this.head
	if this.head != nil {
		this.head.prev = element
	}
	this.head = element
	if this.tail == nil {
		this.tail = this.head
	}
	// fmt.Printf("e.prev %+v e.next %+v tail %+v head %+v\n", element.prev, element.next, this.tail, this.head)
}

func (this *LRUCache) addToCache(key int, value int) {
	element, ok := this.cache[key]
	if ok {
		// Element existed, update value
		element.value = value
		this.removeElement(element)
		this.addElement(element)
	} else {
		if len(this.cache) >= this.capacity {
			// Remove from the cache
			if this.tail != nil {
				delete(this.cache, this.tail.key)
			}
			// Invalidate the tail
			this.removeElement(this.tail)
		}
		new_element := LRUElement{key, value, nil, nil}
		this.addElement(&new_element)
		// Add to the cache
		this.cache[new_element.key] = &new_element
	}
}

func main() {
	cache := Constructor(2)
	cache.Put(1, 1)
	cache.Put(2, 2)
	fmt.Println("1", cache.Get(1))
	cache.Put(3, 3)
	fmt.Println("-1", cache.Get(2))
	cache.Put(4, 4)
	fmt.Println("-1", cache.Get(1))
	fmt.Println("3", cache.Get(3))
	fmt.Println("4", cache.Get(4))
}

package main

import (
    "fmt"
    "math"
)

/*
Given a non-empty binary search tree and a target value, find the value in the BST that is closest to the target.

Note:

Given target value is a floating point.
You are guaranteed to have only one unique value in the BST that is closest to the target.
 */

type TreeNode struct {
    Val int
    Left *TreeNode
    Right *TreeNode
}

/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func closestValue(root *TreeNode, target float64) int {
    nodes := make([]int, 0)
    buildList(root, target, &nodes)

    if len(nodes) == 0 {
        return -1
    }

    valDiff := struct {
        v int
        d float64}{v: nodes[0], d: math.Abs(target - float64(nodes[0]))}

    for _, n := range nodes[1:] {
        d := math.Abs(target - float64(n))
        if d < valDiff.d {
            valDiff.v = n
            valDiff.d = d
        }
    }

    return valDiff.v
}

func buildList(node *TreeNode, target float64, nodes *[]int) {
    if node == nil {
        return
    }
    buildList(node.Left, target, nodes)

    *nodes = append(*nodes, node.Val)
    if target < float64(node.Val) {
        return
    }

    buildList(node.Right, target, nodes)
}

func buildBST(list []int) *TreeNode {
    var node *TreeNode = nil
    for _, x := range list {
        node = insertNode(node, x)
    }
    return node
}

func insertNode(root *TreeNode, val int) *TreeNode {
    if root == nil {
        root = &TreeNode{val, nil, nil}
    } else if val == root.Val {
        root.Val = val
    } else if val < root.Val {
        // left side we're at
        root.Left = insertNode(root.Left, val)
    } else if val > root.Val {
        // right side we're at
        root.Right = insertNode(root.Right, val)
    }
    return root
}

func main() {
    fmt.Println(closestValue(buildBST([]int{4, 2, 5, 1, 3}), 3.714286))
    fmt.Println(closestValue(buildBST([]int{}), -1))
}
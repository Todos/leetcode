package main

// Design and implement a data structure for Least Recently Used (LRU) cache. It should support the following operations: get and put.
//
// get(key) - Get the value (will always be positive) of the key if the key exists in the cache, otherwise return -1.
// put(key, value) - Set or insert the value if the key is not already present. When the cache reached its capacity, it should invalidate the least recently used item before inserting a new item.
//
// Follow up:
// Could you do both operations in O(1) time complexity?
//
// Example:
//
// LRUCache cache = new LRUCache( 2 /* capacity */ );
//
// cache.put(1, 1);
// cache.put(2, 2);
// cache.get(1);       // returns 1
// cache.put(3, 3);    // evicts key 2
// cache.get(2);       // returns -1 (not found)
// cache.put(4, 4);    // evicts key 1
// cache.get(1);       // returns -1 (not found)
// cache.get(3);       // returns 3
// cache.get(4);       // returns 4

import (
	"fmt"
)

type LRUElement struct {
	key int
	age int
}

func (this *LRUElement) Equal(element *LRUElement) bool {
	return this.key == element.key
}

func lruSearch(lruArray []LRUElement, element LRUElement) int {
	result := -1
	for i := 0; i < len(lruArray); i++ {
		if lruArray[i].Equal(&element) {
			// found
			result = i
			break
		}
	}
	return result
}

func lruUpdate(lruArray []LRUElement, element LRUElement) []LRUElement {
	l := len(lruArray)
	if l == 0 {
		return []LRUElement{element}
	}

	pos := lruSearch(lruArray, element)
	if pos < 0 {
		// not found
	} else if pos == 0 {
		lruArray = lruArray[1:]
	} else {
		lruArray = append(lruArray[:pos], lruArray[pos+1:]...)
	}
	return append(lruArray, element)
}

type LRUCache struct {
	capacity int
	cache    map[int]int
	lru      []LRUElement
	age      int
}

func Constructor(capacity int) LRUCache {
	return LRUCache{capacity, make(map[int]int), []LRUElement{}, 0}
}

func (this *LRUCache) Get(key int) int {
	value, ok := this.cache[key]
	if !ok {
		return -1
	}

	this.updateAge(key)
	return value
}

func (this *LRUCache) Put(key int, value int) {
	_, ok := this.cache[key]
	if !ok {
		this.invalidateLru()
	}
	this.cache[key] = value
	this.updateAge(key)
}

func (this *LRUCache) invalidateLru() {
	if len(this.lru) == 0 {
		return
	}
	if len(this.cache) > this.capacity-1 {
		delete(this.cache, this.lru[0].key)
		this.lru = this.lru[1:]
	}
}

func (this *LRUCache) updateAge(key int) {
	this.age++
	this.lru = lruUpdate(this.lru, LRUElement{key, this.age})
}

func main() {
	//lru := []LRUElement{LRUElement{1, 10}, LRUElement{4, 40}, LRUElement{9, 90}}
	//fmt.Println(lruUpdate(lru, LRUElement{5, 100}))

	//lru = []LRUElement{LRUElement{1, 10}, LRUElement{4, 40}, LRUElement{9, 90}}
	//fmt.Println(lruUpdate(lru, LRUElement{1, 100}))

	//lru = []LRUElement{LRUElement{1, 10}, LRUElement{4, 40}, LRUElement{9, 90}}
	//fmt.Println(lruUpdate(lru, LRUElement{9, 100}))

	//lru = []LRUElement{LRUElement{1, 10}, LRUElement{4, 40}, LRUElement{9, 90}}
	//fmt.Println(lruUpdate(lru, LRUElement{4, 100}))

	cache := Constructor(2)
	cache.Put(1, 1)
	cache.Put(2, 2)
	fmt.Println("1", cache.Get(1))
	cache.Put(3, 3)
	fmt.Println("-1", cache.Get(2))
	cache.Put(4, 4)
	fmt.Println("-1", cache.Get(1))
	fmt.Println("3", cache.Get(3))
	fmt.Println("4", cache.Get(4))
}

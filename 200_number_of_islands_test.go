package main

import (
	"fmt"
	"testing"
)

var data = []struct{
	in [][]byte
	expected int
} {
	{in: [][]byte{[]byte{'1', '1', '1', '1', '0'}, []byte{'1', '1', '0', '1', '0'}, []byte{'1', '1', '0', '0', '0'}, []byte{'0', '0', '0', '0', '0'}}, expected: 1},
	{in: [][]byte{[]byte{'1', '1', '0', '0', '0'}, []byte{'1', '1', '0', '0', '0'}, []byte{'0', '0', '1', '0', '0'}, []byte{'0', '0', '0', '1', '1'}}, expected: 3},
}

func Test1(t *testing.T) {
	for _, d := range data {
		input := fmt.Sprintf("%s", d.in)
		result := numIslands(d.in)
		if result != d.expected {
			t.Errorf("For %s expected %d got %d", input, d.expected, result)
		}
	}
}

package main

import (
    "fmt"
    "testing"
)

func getSlice(l *ListNode) []int {
    if l == nil {
        return []int{}
    }

    result := []int{}
    for {
        result = append(result, l.Val)
        if l.Next == nil {
            break
        }

        l = l.Next
    }
    return result
}

func getList(s []int) *ListNode {
    if len(s) == 0 {
        return nil
    }

    head := &ListNode{Val: s[0], Next: nil}
    tail := head
    for _, e := range s[1:] {
        ln := ListNode{Val: e}
        tail.Next = &ln
        tail = &ln
    }
    return head
}

func printList(l *ListNode) {
    for {
        fmt.Printf("%v, ", l)
        if l.Next == nil {
            break
        }

        l = l.Next
    }
    fmt.Printf("\n")
}

func TestList1(t *testing.T) {
    printList(getList([]int{10, 100, 1000}))
    fmt.Println(getSlice(getList([]int{10, 100, 1000})))
    fmt.Println(getSlice(getList([]int{})))
    fmt.Println(getSlice(getList([]int{1001})))
    fmt.Println(getSlice(getList([]int{1001, 1001, 1001})))
}

func TestMergeTwoSortedLists(t *testing.T) {
    printList(mergeTwoLists(getList([]int{1, 2, 4}), getList([]int{1, 3, 4})))
    printList(mergeTwoLists(getList([]int{1, 1, 2}), getList([]int{1, 2, 3})))
    printList(mergeTwoLists(getList([]int{1}), getList([]int{1, 2, 3})))
    printList(mergeTwoLists(getList([]int{1}), getList([]int{1})))
    printList(mergeTwoLists(getList([]int{10}), getList([]int{1})))
    printList(mergeTwoLists(getList([]int{10}), getList([]int{1,11})))
    printList(mergeTwoLists(getList([]int{5}), getList([]int{1,2,4})))
}


package main

import "testing"

var data = []struct {
    nums []int
    target int
    result []int
}{
    {nums: []int{2, 7, 11, 15}, target: 9, result: []int{0, 1}},
    {nums: []int{2, 7, 7, 15}, target: 14, result: []int{1, 2}},
    {nums: []int{2, 7, 7, 7, 15}, target: 14, result: []int{1, 2}},
    {nums: []int{2, 7, 7, 7, 15}, target: 22, result: []int{1, 4}},
    {nums: []int{}, target: 0, result: []int{}},
    {nums: []int{6, 5, 7, 8, 9, 3}, target: 10, result: []int{2, 5}},
}

func Test2Sum(t *testing.T) {
    for _, d := range data {
        r := twoSum(d.nums, d.target)
        if len(d.result) == 0 {
            continue
        }

        if r[0] != d.result[0] || r[1] != d.result[1] {
            t.Error("Nums ", d.nums, "target ", d.target, "expected ", d.result, "actual ", r)
        }

        //t.Log("Nums ", d.nums, "target ", d.target, "expected ", d.result, "actual ", r)
    }
}

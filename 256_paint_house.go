package main

import (
	"fmt"
)

/*
There are a row of n houses, each house can be painted with one of the three colors: red, blue or green. The cost of painting each house with a certain color is different. You have to paint all the houses such that no two adjacent houses have the same color.

The cost of painting each house with a certain color is represented by a n x 3 cost matrix. For example, costs[0][0] is the cost of painting house 0 with color red; costs[1][2] is the cost of painting house 1 with color green, and so on... Find the minimum cost to paint all houses.

Note:
All costs are positive integers.

Example:

Input: [[17,2,17],[16,16,5],[14,3,19]]
Output: 10
Explanation: Paint house 0 into blue, paint house 1 into green, paint house 2 into blue.
			 Minimum cost: 2 + 5 + 3 = 10.
*/

func minCost(costs [][]int) int {
	if len(costs) == 0 {
		return 0
	}

	for i := 1; i < len(costs); i++ {
		costs[i][0] += min(costs[i-1][1], costs[i-1][2])
		costs[i][1] += min(costs[i-1][0], costs[i-1][2])
		costs[i][2] += min(costs[i-1][0], costs[i-1][1])
	}

	last := costs[len(costs) - 1]
	return min(last[0], min(last[1], last[2]))
}

func min(x int, y int) int {
	if x > y {
		return y
	} else {
		return x
	}
}

func main() {
	//i1 := [][]int{[]int{17, 2, 17}, []int{16, 16, 5}, []int{14, 3, 19}}
	//i1 := [][]int{[]int{1, 2, 3}, []int{4, 5, 6}, []int{7, 8, 9}}
	//i1 := [][]int{[]int{12,1,19},[]int{15,1,10},[]int{3,11,10},[]int{9,3,10},[]int{4,8,7},[]int{4,18,2},[]int{16,6,6},[]int{3,3,6},[]int{10,18,16},[]int{5,4,8},[]int{5,3,16},[]int{11,8,19},[]int{18,15,18},[]int{16,4,15},[]int{10,7,13},[]int{11,10,14},[]int{3,9,8},[]int{5,2,2},[]int{3,2,5},[]int{2,19,14},[]int{17,3,6},[]int{6,4,17},[]int{5,15,19},[]int{2,14,14},[]int{19,4,16}}
	i1 := [][]int{[]int{13,12,19},[]int{17,19,3},[]int{16,11,10},[]int{11,6,19},[]int{5,8,20},[]int{11,4,3},[]int{3,17,6},[]int{11,3,19},[]int{15,16,5},[]int{12,8,13},[]int{8,7,20},[]int{11,8,17},[]int{17,8,4},[    ]int{14,7,17},[]int{15,7,9},[]int{20,9,15},[]int{14,7,10},[]int{7,18,9},[]int{15,4,17},[]int{12,16,15},[]int{14,11,7},[]int{10,12,20},[]int{7,18,9},[]int{18,3,19}}
	fmt.Println(minCost(i1))
}

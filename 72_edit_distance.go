package main

// Given two words word1 and word2, find the minimum number of operations required to convert word1 to word2.
//
// You have the following 3 operations permitted on a word:
//
//     Insert a character
//     Delete a character
//     Replace a character

func minDistance(word1 string, word2 string) int {
	memo := make(map[[2]int]int)
	result := minDistanceRaw(word1, word2, 0, 0, memo)
	return result
}

func minInt(v1 int, vn ...int) (result int) {
	result = v1
	for i := 0; i < len(vn); i++ {
		if vn[i] < result {
			result = vn[i]
		}
	}
	return
}

func minDistanceRaw(word1 string, word2 string, i int, j int, memo map[[2]int]int) int {
	if i == len(word1) && j == len(word2) {
		return 0
	}
	if i == len(word1) {
		return len(word2) - j
	}
	if j == len(word2) {
		return len(word1) - i
	}
	_, ok := memo[[2]int{i, j}]
	if !ok {
		var answer int
		if word1[i] == word2[j] {
			answer = minDistanceRaw(word1, word2, i+1, j+1, memo)
		} else {
			insertOp := 1 + minDistanceRaw(word1, word2, i, j+1, memo)
			deleteOp := 1 + minDistanceRaw(word1, word2, i+1, j, memo)
			replaceOp := 1 + minDistanceRaw(word1, word2, i+1, j+1, memo)
			answer = minInt(insertOp, deleteOp, replaceOp)
		}
		memo[[2]int{i, j}] = answer
	}
	return memo[[2]int{i, j}]
}

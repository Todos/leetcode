package main

import "fmt"

/*
Given a 2d grid map of '1's (land) and '0's (water), count the number of islands. An island is surrounded by water and
is formed by connecting adjacent lands horizontally or vertically. You may assume all four edges of the grid are all
surrounded by water.

Example 1:

Input:
11110
11010
11000
00000

Output: 1
Example 2:

Input:
11000
11000
00100
00011

Output: 3
 */

func numIslands(grid [][]byte) int {
	result := 0
	for i := 0; i < len(grid); i++ {
		for j := 0; j < len(grid[i]); j++ {
			if grid[i][j] == '1' {
				result += 1
				dfs(&grid, i, j)
			}
		}
	}
	return result
}

func dfs(grid *[][]byte, i int, j int) {
	if i < 0 || i >= len(*grid) || j < 0 || j >= len((*grid)[i]) {
		return
	}

	if (*grid)[i][j] == '1' {
		(*grid)[i][j] = '0'

		dfs(grid, i+1, j)
		dfs(grid, i, j+1)
		dfs(grid, i-1, j)
		dfs(grid, i, j-1)
	}
}

func main() {
	data := [][]byte{[]byte{'1', '1', '0'}, []byte{'1', '0', '0'}, []byte{'0', '0', '1'}}
	fmt.Println(numIslands(data))

	data = [][]byte{[]byte{'1', '1', '1', '1', '0'}, []byte{'1', '1', '0', '1', '0'}, []byte{'1', '1', '0', '0', '0'}, []byte{'0', '0', '0', '0', '0'}}
	fmt.Println(numIslands(data))
}

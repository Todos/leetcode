package main

import (
	"testing"
)

func Test1(t *testing.T) {
	if 1 != numTrees(-3) {
		t.Fail()
	}
	if 1 != numTrees(0) {
		t.Fail()
	}
}

func Test2(t *testing.T) {
	result := numTrees(3)
	if 5 != result {
		t.Error(5, "!=", result)
	}
}

func Test3(t *testing.T) {
	result := numTrees(5)
	if 42 != result {
		t.Error(42, "!=", result)
	}
}

func Test4(t *testing.T) {
	result := numTrees(10)
	if 16796 != result {
		t.Error(16796, "!=", result)
	}
}

func Test5(t *testing.T) {
	result := numTrees(12)
	if 208012 != result {
		t.Error(208012, "!=", result)
	}
}

func Test6(t *testing.T) {
	result := numTrees(15)
	if 9694845 != result {
		t.Error(9694845, "!=", result)
	}
}

func Test7(t *testing.T) {
	result := numTrees(16)
	if 35357670 != result {
		t.Error(35357670, "!=", result)
	}
}

func Test8(t *testing.T) {
	result := numTrees(19)
	if 1767263190 != result {
		t.Error(1767263190, "!=", result)
	}
}

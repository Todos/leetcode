package main


/*
You are given two non-empty linked lists representing two non-negative integers.
The digits are stored in reverse order and each of their nodes contain a single digit.
Add the two numbers and return it as a linked list.

You may assume the two numbers do not contain any leading zero, except the number 0 itself.

Example:

Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
Output: 7 -> 0 -> 8
Explanation: 342 + 465 = 807.
 */

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */

type ListNode struct {
    Val int
    Next *ListNode
}

func addTwoNumbers(l1 *ListNode, l2 *ListNode) *ListNode {
    var (
        sum int
        carry bool
        head *ListNode
        tail *ListNode
    )

    for {
        if l1 == nil && l2 == nil && carry == false {
            break
        }

        var (
            n1, n2 int
        )

        if l1 == nil {
            n1 = 0
        } else {
            n1 = l1.Val
            l1 = l1.Next
        }

        if l2 == nil {
            n2 = 0
        } else {
            n2 = l2.Val
            l2 = l2.Next
        }

        sum = n1 + n2
        if carry {
            sum += 1
        }
        if sum > 9 {
            carry = true
            sum = sum - 10
        } else {
            carry = false
        }

        node := ListNode{Val: sum}
        if head == nil {
            head = &node
            tail = &node
        } else {
            tail.Next = &node
            tail = &node
        }
    }

    return head
}


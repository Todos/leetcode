package main

import "testing"

var data = []struct{
	in []int
	out string
}{
	{[]int{10, 2}, "210"},
	{[]int{3, 30, 34, 5, 9}, "9534330"},
	{[]int{1}, "1"},
	{[]int{10}, "10"},
	{[]int{0, 0}, "0"},
}

func Test1(t *testing.T) {
	for _, d := range data {
		res := largestNumber(d.in)
		if res != d.out {
			t.Error("For", d.in, "got", res, "expected", d.out)
		}
	}
}

package main

import "testing"

type testpair struct {
	input  string
	result int
}

var tests = []testpair{
	{"", 0},
	{"(", 0},
	{")", 0},
	{"()", 2},
	{"(()", 2},
	{"())", 2},
	{")()())", 4},
	{"()(()", 2}, // failed during evaluation
	{"())()", 2}, // failed during evaluation
	{"(((())))((", 8},
	{"((()))((((()))))(())", 20},
	{"()(((()", 2},
	{"())))()", 2},
	{"((((((((((()))))))))))", 22},
	{"))))))))()((((((", 2},
	{")()())()()(", 4},
	{"(()))())(", 4},
	{"((()))())", 8},
	{")()))()(", 2},
	{"()((())()", 6},
}

func TestLongestValidParentheses(t *testing.T) {
	for _, pair := range tests {
		res := longestValidParentheses(pair.input)
		if pair.result != res {
			t.Error(
				"For", pair.input,
				"expected", pair.result,
				"got", res,
			)
		}
	}
}

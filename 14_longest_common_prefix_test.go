package main

import "testing"

var data = []struct {
	in []string
	out string
} {
	{in: []string{"flower", "flow", "flight"}, out: "fl"},
	{in: []string{"dog", "racecar", "car"}, out: ""},
	{in: []string{""}, out: ""},
	{in: []string{"ab", "a"}, out: "a"},
}

func TestLongestCommonPrefix(t *testing.T) {
	for _, d := range data {
		res := longestCommonPrefix(d.in)
		if res != d.out {
			t.Error("For", d.in, "got", res, "expected", d.out)
		}
	}
}
package main

import "testing"

var data = []struct{
	intervals [][]int
	res int
} {
	{[][]int{{0, 30}, {5, 10}, {15, 20}}, 2},
	{[][]int{{7, 10}, {2, 4}}, 1},
	{[][]int{{13, 15}, {1, 13}}, 1},
	{[][]int{{1, 5}, {8, 9}, {8, 9}}, 2},
	{[][]int{{8, 16}, {3, 12}, {3, 11}}, 3},
	{[][]int{{2, 11}, {6, 16}, {11, 16}}, 2},
}

func Test1(t *testing.T) {
	for _, x := range data {
		res := minMeetingRooms(x.intervals)
		if res != x.res {
			t.Error("For", x.intervals, "got", res, "expected", x.res)
		}
	}
}

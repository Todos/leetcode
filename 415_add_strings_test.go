package main

import "testing"

var table = []struct {
    num1 string
    num2 string
    res string
} {
    {"12", "4", "16"},
    {"12", "14", "26"},
    {"", "14", "14"},
    {"12", "", "12"},
    {"", "", ""},
    {"99", "1", "100"},
    {"999", "11", "1010"},
    {"0", "0", "0"},
    {"6", "501", "507"},
}

func Test1(t *testing.T) {
    for _, d := range table {
        res := addStrings(d.num1, d.num2)
        if res != d.res {
            t.Error("For", d.num1, "and", d.num2, "got", res, "expected", d.res)
        }
    }
}

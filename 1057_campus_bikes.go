package main

import (
    "math"
    "sort"
)

/*
On a campus represented as a 2D grid, there are N workers and M bikes, with N <= M. Each worker and bike is a 2D
coordinate on this grid.

Our goal is to assign a bike to each worker. Among the available bikes and workers, we choose the (worker, bike) pair
with the shortest Manhattan distance between each other, and assign the bike to that worker.
(If there are multiple (worker, bike) pairs with the same shortest Manhattan distance, we choose the pair with the
smallest worker index; if there are multiple ways to do that, we choose the pair with the smallest bike index).
We repeat this process until there are no available workers.

The Manhattan distance between two points p1 and p2 is Manhattan(p1, p2) = |p1.x - p2.x| + |p1.y - p2.y|.

Return a vector ans of length N, where ans[i] is the index (0-indexed) of the bike that the i-th worker is assigned to.
 */

func manhattanDistance(x1 int, x2 int, y1 int, y2 int) float64 {
    return math.Abs(float64(x1) - float64(x2)) + math.Abs(float64(y1) - float64(y2))
}

type slot struct {
    w int
    b int
    dist float64
}

func assignBikes(workers [][]int, bikes [][]int) []int {
    manhDist := make([]slot, 0)
    for i, wk := range workers {
        for j, bk := range bikes {
            md := manhattanDistance(wk[0], bk[0], wk[1], bk[1])
            manhDist = append(manhDist, slot{w: i, b: j, dist: md})
        }
    }
    sort.SliceStable(manhDist, func(i, j int) bool {
        mi, mj := manhDist[i], manhDist[j]
        if mi.dist != mj.dist {
            return mi.dist < mj.dist
        }

        if mi.w != mj.w {
            return mi.w < mj.w
        }

        return mi.b < mj.b
    })

    uworkers := make(map[int]int)
    ubikes := make(map[int]int)

    for _, sl := range manhDist {
        _, ok := uworkers[sl.w]
        if ok {
            continue
        }
        _, ok = ubikes[sl.b]
        if ok {
            continue
        }

        uworkers[sl.w] = sl.b
        ubikes[sl.b] = sl.w
    }

    res := make([]int, 0)
    for i := 0; i < len(workers); i++ {
        res = append(res, uworkers[i])
    }

    return res
}

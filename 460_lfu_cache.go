package main

// Design and implement a data structure for Least Frequently Used (LFU) cache. It should support the following operations: get and put.

// get(key) - Get the value (will always be positive) of the key if the key exists in the cache, otherwise return -1.
// put(key, value) - Set or insert the value if the key is not already present. When the cache reaches its capacity, it should invalidate the least frequently used item before inserting a new item. For the purpose of this problem, when there is a tie (i.e., two or more keys that have the same frequency), the least recently used key would be evicted.
//
// Follow up:
// Could you do both operations in O(1) time complexity?

// Example:
//
// LFUCache cache = new LFUCache( 2 /* capacity */ );
//
// cache.put(1, 1);
// cache.put(2, 2);
// cache.get(1);       // returns 1
// cache.put(3, 3);    // evicts key 2
// cache.get(2);       // returns -1 (not found)
// cache.get(3);       // returns 3.
// cache.put(4, 4);    // evicts key 1.
// cache.get(1);       // returns -1 (not found)
// cache.get(3);       // returns 3
// cache.get(4);       // returns 4

// The solution is based on http://dhruvbird.com/lfu.pdf article:
// An O(1) algorithm for implementing the LFU cache eviction scheme
// Prof. Ketan Shah, Anirban Mitra, Dhruv Matani, August 16, 2010

import (
	"fmt"
)

type LRUElement struct {
	key    int
	data   int
	parent *LFUElement
	prev   *LRUElement
	next   *LRUElement
}

type LRUCache struct {
	capacity int
	cache    map[int]*LRUElement
	head     *LRUElement // Need to effectively add
	tail     *LRUElement // Need to effectively remove
}

type LFUElement struct {
	freq      int
	lru_cache *LRUCache
	prev      *LFUElement
	next      *LFUElement
}

type LFUCache struct {
	capacity     int
	lru_elements map[int]*LRUElement
	head         *LFUElement // Need to effectively add
	//tail         *LFUElement // Need to effectively remove
}

func PrintLFUCache(cache *LFUCache) {
	fmt.Printf(">>> Cache c:%d, l:%d, h:%#v\n", cache.capacity, len(cache.lru_elements), cache.head)
	fmt.Printf(">>> LRU elements:\n")
	for _, el := range cache.lru_elements {
		fmt.Printf("\t>>> %p -> %#v\n", el, el)
	}
	fmt.Printf(">>> LFU elements:\n")
	el := cache.head
	for {
		fmt.Printf("\t>>> %p -> %#v\n", el, el)
		for _, elr := range el.lru_cache.cache {
			fmt.Printf("\t\t>>> %p -> %#v\n", elr, elr)
		}

		el = el.next
		if el == nil {
			break
		}
		fmt.Printf(", ")
	}
	fmt.Printf("\n\n")
}

func Constructor(capacity int) LFUCache {
	return LFUCache{
		capacity,
		make(map[int]*LRUElement),
		NewLFUElement(0, nil, nil)}
}

func LRUCacheConstructor(capacity int) LRUCache {
	// NEW-LFU-CACHE
	return LRUCache{capacity, make(map[int]*LRUElement), nil, nil}
}

func NewLFUElement(freq int, prev *LFUElement, next *LFUElement) *LFUElement {
	// GET-NEW-NODE
	lru_cache := LRUCacheConstructor(0)
	element := LFUElement{freq, &lru_cache, nil, nil}
	element.prev = prev
	element.next = next
	if prev != nil {
		prev.next = &element
	}
	if next != nil {
		next.prev = &element
	}
	return &element
}

func DeleteLFUElement(element *LFUElement) {
	// DELETE-NODE
	if element.prev != nil {
		element.prev.next = element.next
	}
	if element.next != nil {
		element.next.prev = element.prev
	}
}

func NewLRUElement(key int, data int, parent *LFUElement) *LRUElement {
	// NEW-LFU-ITEM
	element := LRUElement{key, data, parent, nil, nil}
	return &element
}

// *** LFU Cache ***
func (this *LFUCache) Get(key int) int {
	// ACCESS (key)
	lru_element, ok := this.lru_elements[key]
	if !ok {
		return -1
	}
	lfu_element := lru_element.parent
	next_lfu_element := lfu_element.next
	if next_lfu_element == nil || next_lfu_element == this.head || next_lfu_element.freq != lfu_element.freq+1 {
		next_lfu_element = NewLFUElement(lfu_element.freq+1, lfu_element, next_lfu_element)
	}
	// Put a copy of the struct
	tmp_lru_element := *lru_element
	next_lfu_element.lru_cache.Put(&tmp_lru_element)
	lru_element.parent = next_lfu_element

	lfu_element.lru_cache.Evict(lru_element.key)
	if lfu_element.lru_cache.Size() == 0 {
		DeleteLFUElement(lfu_element)
	}
	fmt.Printf("*** GET %d ***\n", key)
	//PrintLFUCache(this)
	return lru_element.data
}

func (this *LFUCache) Put(key int, value int) {
	// INSERT(key, value)
	if this.capacity <= 0 {
		// Corner case
		return
	}
	data := this.Get(key)
	if data >= 0 {
		// Element existed
		lru_element, _ := this.lru_elements[key]
		lru_element.data = value
		return

	}
	if len(this.lru_elements) >= this.capacity {
		if this.head.next != nil {
			lfu_lru_cache := this.head.next.lru_cache
			tail := lfu_lru_cache.tail
			if tail != nil {
				lfu_lru_cache.Evict(tail.key)
				// Remove from the cache
				delete(this.lru_elements, tail.key)
			}
		}
	}
	lfu_element := this.head.next
	if lfu_element == nil || lfu_element.freq != 1 {
		lfu_element = NewLFUElement(1, this.head, lfu_element)
	}
	lru_element := NewLRUElement(key, value, lfu_element)
	// Put a copy of the struct
	tmp_lru_element := *lru_element
	lfu_element.lru_cache.Put(&tmp_lru_element)
	this.lru_elements[key] = lru_element
	fmt.Printf("*** PUT %d %d ***\n", key, value)
	//PrintLFUCache(this)
	return
}

// *** LRU Cache ***
func (this *LRUCache) Get(key int) (*LRUElement, bool) {
	element, ok := this.cache[key]
	if !ok {
		return nil, false
	}

	this.removeElement(element)
	this.addElement(element)
	return element, true
}

func (this *LRUCache) Put(element *LRUElement) {
	cached, ok := this.cache[element.key]
	if ok {
		// Element existed, update data
		cached.data = element.data
		this.removeElement(cached)
		this.addElement(cached)
	} else {
		// Capacity 0 is reserved value for unset cache capacity
		if this.capacity != 0 && len(this.cache) >= this.capacity {
			this.Evict(this.tail.key)
		}
		this.addElement(element)
		// Add to the cache
		this.cache[element.key] = element
	}
}

func (this *LRUCache) Evict(key int) int {
	cached, ok := this.cache[key]
	if !ok {
		return -1
	}
	// Remove from the cache
	delete(this.cache, cached.key)

	// Remove from doubly linked list
	this.removeElement(cached)
	return 0
}

func (this *LRUCache) Size() int {
	return len(this.cache)
}

func (this *LRUCache) removeElement(element *LRUElement) {
	if element.prev != nil {
		element.prev.next = element.next
	} else {
		this.head = element.next
	}
	if element.next != nil {
		element.next.prev = element.prev
	} else {
		this.tail = element.prev
	}
}

func (this *LRUCache) addElement(element *LRUElement) {
	element.prev = nil
	element.next = this.head
	if this.head != nil {
		this.head.prev = element
	}
	this.head = element
	if this.tail == nil {
		this.tail = this.head
	}
}

func main() {
	// cache := Constructor(2)
	// cache.Put(1, 1)
	// cache.Put(2, 2)
	// fmt.Println("1", cache.Get(1))
	// cache.Put(3, 3)
	// fmt.Println("-1", cache.Get(2))
	// fmt.Println("3", cache.Get(3))
	// cache.Put(4, 4)
	// fmt.Println("-1", cache.Get(1))
	// fmt.Println("3", cache.Get(3))
	// fmt.Println("4", cache.Get(4))

	// cache := Constructor(2)
	// cache.Put(3, 1)
	// cache.Put(2, 1)
	// cache.Put(2, 2)
	// cache.Put(4, 4)
	// fmt.Println("2", cache.Get(2))

	// cache := Constructor(0)
	// cache.Put(0, 0)
	// fmt.Println("-1", cache.Get(0))

	// cache = Constructor(1)
	// cache.Put(0, 0)
	// fmt.Println("0", cache.Get(0))

	cache := Constructor(1)
	cache.Put(2, 1)
	fmt.Println("1", cache.Get(2))
}

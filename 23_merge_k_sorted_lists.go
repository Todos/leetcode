package main

// Merge k sorted linked lists and return it as one sorted list. Analyze and describe its complexity.

import (
	"fmt"
)

type ListNode struct {
	Val  int
	Next *ListNode
}

func printList(list []*ListNode) {
	fmt.Printf("[\n")
	for index, head := range list {
		fmt.Printf("%d: ", index)
		node := head
		for {
			fmt.Printf("\t%p->%#v, ", node, node)
			node = node.Next
			if node == nil {
				break
			}
		}
		fmt.Printf("\n")
	}
	fmt.Printf("]\n")
}

func printSLL(sll *ListNode) {
	if sll == nil {
		return
	}

	node := sll
	for {
		fmt.Printf("\t%p->%#v, ", node, node)
		node = node.Next
		if node == nil {
			break
		}
	}
	fmt.Printf("\n")
}

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func mergeKLists(lists []*ListNode) *ListNode {
	if len(lists) == 0 {
		return nil
	}

	var resultSLL *ListNode = nil
	var tailSLL *ListNode = nil
	var maxInt int = 1<<31 - 1
	for {
		var minVal int = maxInt
		var minInd int = 0
		var foundMin bool = false
		for i := 0; i < len(lists); i++ {
			if lists[i] != nil && lists[i].Val < minVal {
				minVal = lists[i].Val
				minInd = i
				foundMin = true
			}
		}

		// append to the result
		var node ListNode = ListNode{minVal, nil}
		if foundMin == true {
			if resultSLL != nil {
				tailSLL.Next = &node
				tailSLL = &node
			} else {
				resultSLL = &node
				tailSLL = &node
			}
		}

		// remove the head from the linked list
		if len(lists) > 0 && lists[minInd] != nil {
			lists[minInd] = lists[minInd].Next
		}

		// remove empty linked list
		var i int = 0
		for {
			if i >= len(lists) {
				break
			}
			if lists[i] == nil {
				lists = append(lists[:i], lists[i+1:]...)
			}
			i++
		}

		// we're done
		if len(lists) == 0 {
			break
		}
	}

	return resultSLL
}

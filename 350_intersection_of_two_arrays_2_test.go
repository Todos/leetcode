package main

import (
    "reflect"
    "sort"
    "testing"
)

var data = []struct {
    nums1 []int
    nums2 []int
    res []int
}{
    {[]int{1, 2, 2, 1}, []int{2, 2}, []int{2, 2}},
    {[]int{4, 9, 5}, []int{9, 4, 9, 8, 4}, []int{4, 9}},
    {[]int{1, 1}, []int{}, []int{}},
    {[]int{1}, []int{1}, []int{1}},
    {[]int{1, 2}, []int{1, 1}, []int{1}},
    {[]int{2, 1}, []int{1, 2}, []int{1, 2}},
    {[]int{3, 1, 2}, []int{1, 3}, []int{1, 3}},
}

func Test1(t *testing.T) {
    for _, d := range data {
        res := intersect(d.nums1, d.nums2)
        sort.Ints(res)
        sort.Ints(d.res)
        if !reflect.DeepEqual(res, d.res) {
            t.Error("For", d.nums1, "and", d.nums2, "got", res, "expected", d.res)
        }
    }
}

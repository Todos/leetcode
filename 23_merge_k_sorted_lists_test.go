package main

/*
➜  leetcode git:(master) go test -v 23_merge_k_sorted_lists.go 23_merge_k_sorted_lists_test.go
=== RUN   Test1
--- PASS: Test1 (0.00s)
=== RUN   Test2
--- PASS: Test2 (0.00s)
=== RUN   Test3
--- PASS: Test3 (0.00s)
=== RUN   Test4
--- PASS: Test4 (0.00s)
=== RUN   Test5
--- PASS: Test5 (0.00s)
=== RUN   Test6
--- PASS: Test6 (0.00s)
=== RUN   Test7
--- PASS: Test7 (0.00s)
=== RUN   Test8
--- PASS: Test8 (0.00s)
=== RUN   Test9
--- FAIL: Test9 (0.00s)
	23_merge_k_sorted_lists_test.go:139: Expected empty list &{0 <nil>}
=== RUN   Test10
--- FAIL: Test10 (0.00s)
	23_merge_k_sorted_lists_test.go:147: Expected empty list &{0 0x18e76590}
=== RUN   Test11
--- FAIL: Test11 (0.00s)
	23_merge_k_sorted_lists_test.go:155: Expected empty list &{0 0x18e765f8}
FAIL
FAIL	command-line-arguments	0.013s
➜  leetcode git:(master)

*/

import (
	"sort"
	"testing"
)

func genList(ints [][]int) (result []*ListNode) {
	for i := 0; i < len(ints); i++ {
		if len(ints[i]) < 1 {
			continue
		}
		var prevNode *ListNode = &ListNode{ints[i][0], nil}
		result = append(result, prevNode)
		for j := 1; j < len(ints[i]); j++ {
			var node ListNode = ListNode{ints[i][j], nil}
			prevNode.Next = &node
			prevNode = &node
		}
	}
	return
}

func checkResult(t *testing.T, data [][]int, result *ListNode) {
	var data_slice []int = []int{}
	for i := 0; i < len(data); i++ {
		for j := 0; j < len(data[i]); j++ {
			data_slice = append(data_slice, data[i][j])
		}
	}
	sort.Ints(data_slice)
	var res_slice []int = []int{}
	var node *ListNode = result
	for {
		if node == nil {
			break
		}
		res_slice = append(res_slice, node.Val)
		if node.Next == nil {
			break
		}
		node = node.Next
	}
	sort.Ints(res_slice)

	if len(data_slice) != len(res_slice) {
		t.Error(
			"For", data_slice,
			"got", res_slice,
		)
		return
	}

	for i, element := range data_slice {
		if element != res_slice[i] {
			t.Error(
				"For", data_slice,
				"got", res_slice,
			)
		}
	}
}

func runAndCheckResult(t *testing.T, data [][]int) {
	var list []*ListNode = genList(data)
	result := mergeKLists(list)
	checkResult(t, data, result)
}

func Test1(t *testing.T) {
	result := mergeKLists([]*ListNode{})
	if result != nil {
		t.Error("Expected nil", result)
	}
}

func Test2(t *testing.T) {
	var data = [][]int{
		{10, 20, 30},
	}
	runAndCheckResult(t, data)
}

func Test3(t *testing.T) {
	var data = [][]int{
		{10, 20, 30},
		{5, 20, 50},
	}
	runAndCheckResult(t, data)
}

func Test4(t *testing.T) {
	var data = [][]int{
		{5, 6, 7},
		{5, 20, 50},
	}
	runAndCheckResult(t, data)
}

func Test5(t *testing.T) {
	var data = [][]int{
		{5, 5, 5},
		{5, 5, 5},
	}
	runAndCheckResult(t, data)
}

func Test6(t *testing.T) {
	var data = [][]int{
		{10},
		{10},
		{10},
		{10},
	}
	runAndCheckResult(t, data)
}

func Test7(t *testing.T) {
	var data = [][]int{
		{0},
		{0},
		{0},
		{0},
	}
	runAndCheckResult(t, data)
}

func Test8(t *testing.T) {
	var data = [][]int{
		{},
	}
	runAndCheckResult(t, data)
}

func Test9(t *testing.T) {
	var lists []*ListNode = []*ListNode{{}}
	result := mergeKLists(lists)
	if result != nil {
		t.Error("Expected empty list", result)
	}
}

func Test10(t *testing.T) {
	var lists []*ListNode = []*ListNode{{}, {}}
	result := mergeKLists(lists)
	if result != nil {
		t.Error("Expected empty list", result)
	}
}

func Test11(t *testing.T) {
	var lists []*ListNode = []*ListNode{{}, {}, {}, {}, {}}
	result := mergeKLists(lists)
	if result != nil {
		t.Error("Expected empty list", result)
	}
}

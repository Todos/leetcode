package main

// Given an array of integers A, return the largest integer that only occurs once.
//
// If no integer occurs once, return -1.

func largestUniqueNumber(A []int) int {
    var hmap = make(map[int]int, 0)
    var maxNum = -1 // 0 <= A[i] <= 1000

    for _, a := range A {
        _, ok := hmap[a]
        if ok {
            hmap[a]++
        } else {
            hmap[a] = 1
        }
    }

    for k, v := range hmap {
        if v == 1 && k > maxNum {
            maxNum = k
        }
    }

    return maxNum
}

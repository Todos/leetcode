package main

import (
    "math"
)

// Given a list of words and two words word1 and word2, return the shortest distance between these two words in the list.
//
// Example:
// Assume that words = ["practice", "makes", "perfect", "coding", "makes"].
//
// Input: word1 = “coding”, word2 = “practice”
// Output: 3
// Input: word1 = "makes", word2 = "coding"
// Output: 1
// Note:
// You may assume that word1 does not equal to word2, and word1 and word2 are both in the list.

func shortestDistance(words []string, word1 string, word2 string) int {
    var wmap = make(map[string][]int, 0)
    for idx, v := range words {
        dat, ok := wmap[v]
        if ok {
            dat = append(dat, idx)
            wmap[v] = dat
        } else {
            wmap[v] = []int{idx}
        }
    }

    lst1 := wmap[word1]
    lst2 := wmap[word2]
    shortestDist := math.MaxInt32

    for _, e1 := range lst1 {
        for _, e2 := range lst2 {
            absDiff := Abs(e2 - e1)
            if absDiff < shortestDist {
                shortestDist = absDiff
            }
        }
    }
    return shortestDist
}

func Abs(x int) int {
    if x < 0 {
        return -x
    }
    return x
}
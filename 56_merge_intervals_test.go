package main

import (
	"reflect"
	"testing"
)

var data = []struct {
	in  [][]int
	out [][]int
}{
	{[][]int{{1, 3}, {2, 6}, {8, 10}, {15, 18}}, [][]int{{1, 6}, {8, 10}, {15, 18}}},
	{[][]int{{1, 4}, {4, 5}}, [][]int{{1, 5}}},
	{[][]int{{1, 4}, {0, 4}}, [][]int{{0, 4}}},
}

func Test1(t *testing.T) {
	for _, d := range data {
		res := merge(d.in)
		if !reflect.DeepEqual(res, d.out) {
			t.Error("For", d.in, "expected", d.out, "got", res)
		}
	}
}

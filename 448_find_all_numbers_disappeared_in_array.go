package main

// Given an array of integers where 1 ≤ a[i] ≤ n (n = size of array), some elements appear twice and others appear once.
//
// Find all the elements of [1, n] inclusive that do not appear in this array.
//
// Could you do it without extra space and in O(n) runtime? You may assume the returned list does not count as extra space.
//
// Example:
//
// Input:
// [4,3,2,7,8,2,3,1]
//
// Output:
// [5,6]

import (
	"fmt"
)

func findDisappearedNumbers(nums []int) []int {
	result := []int{}
	if len(nums) <= 1 {
		return result
	}
	verify_map := map[int]int{}
	max_num := nums[0]
	for _, num := range nums {
		if num > max_num {
			max_num = num
		}
		_, ok := verify_map[num]
		if !ok {
			verify_map[num] = 1
		} else {
			verify_map[num] += 1
		}
	}

	if len(nums) > max_num {
		max_num = len(nums)
	}

	for x := 1; x <= max_num; x++ {
		_, ok := verify_map[x]
		if !ok {
			result = append(result, x)
		}
	}
	return result
}

func main() {
	fmt.Println(findDisappearedNumbers([]int{4, 3, 2, 7, 8, 2, 3, 1}))
	fmt.Println("---")
	fmt.Println(findDisappearedNumbers([]int{1, 1}))
}

// ➜  src git:(master) ✗ go run main.go
// [5 6]
// ---
// [2]

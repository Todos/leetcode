package main

import "fmt"

/*
Invert a binary tree.

Example:

Input:

     4
   /   \
  2     7
 / \   / \
1   3 6   9
Output:

     4
   /   \
  7     2
 / \   / \
9   6 3   1
Trivia:
This problem was inspired by this original tweet by Max Howell:

Google: 90% of our engineers use the software you wrote (Homebrew), but you can’t invert a binary tree on a whiteboard so f*** off.
 */

type TreeNode struct {
    Val int
    Left *TreeNode
    Right *TreeNode
}

func invertTree(root *TreeNode) *TreeNode {
	var invertedItems = make(map[**TreeNode]bool, 0)
	traverseInvertBST(root, &invertedItems)
	return root
}

func traverseInvertBST(node *TreeNode, invertedItems *map[**TreeNode]bool) {
	if node == nil {
		return
	}

	_, ok := (*invertedItems)[&node]
	if !ok {
		var tmp = node.Left
		node.Left = node.Right
		node.Right = tmp
		(*invertedItems)[&node] = true
	} else {
		return
	}

	traverseInvertBST(node.Left, invertedItems)
	traverseInvertBST(node.Right, invertedItems)
}

func buildBST(list []int) *TreeNode {
	var node *TreeNode = nil
	for _, x := range list {
		node = insertNode(node, x)
	}
	return node
}

func insertNode(root *TreeNode, val int) *TreeNode {
	if root == nil {
		root = &TreeNode{val, nil, nil}
	} else if val == root.Val {
		root.Val = val
	} else if val < root.Val {
		// left side we're at
		root.Left = insertNode(root.Left, val)
	} else if val > root.Val {
		// right side we're at
		root.Right = insertNode(root.Right, val)
	}
	return root
}

func main() {
	var bst = buildBST([]int{4,2,7,1,3,6,9})
	// result []int{4,7,2,9,6,3,1}

	var newBST = invertTree(bst)
	fmt.Println(newBST)

	bst = buildBST([]int{1,2})
	// result []int{1,null,2}

	newBST = invertTree(bst)
	fmt.Println(newBST)

	bst = &TreeNode{1, &TreeNode{2, nil, nil}, nil}
	newBST = invertTree(bst)
	fmt.Println(newBST)
}

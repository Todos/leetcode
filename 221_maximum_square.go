package main

import "fmt"

/*
Given a 2D binary matrix filled with 0's and 1's, find the largest square containing only 1's and return its area.

Example:

Input:

1 0 1 0 0
1 0 1 1 1
1 1 1 1 1
1 0 0 1 0

Output: 4
 */

func maximalSquare(matrix [][]byte) int {
	result := 0
	for i := 0; i < len(matrix); i++ {
		for j := 0; j < len(matrix[i]); j++ {
			if matrix[i][j] == '1' {
				square := 1
				traverseSquare(matrix, i, j, &square)
				if square > result {
					result = square
				}
			}
		}
	 }
	return result
}

func traverseSquare(matrix [][]byte, i int, j int, square *int) {
	dist := 1
	for {

		if i+dist >= len(matrix) || j+dist >= len(matrix[i]) {
			break
		}

		sideFilled := true
		for k := 0; k < dist; k++ {
			if matrix[i+dist][j+k] == '0' {
				sideFilled = false
				break
			}
		}

		bottomFilled := true
		for k := 0; k < dist; k++ {
			if matrix[i+k][j+dist] == '0' {
				bottomFilled = false
				break
			}
		}

		angleFilled := matrix[i+dist][j+dist] == '1'

		if sideFilled && bottomFilled && angleFilled {
			*square += 1 + dist*2
		} else {
			break
		}

		dist++
	}
}

func main() {
	//data := [][]byte{[]byte{'1', '0', '1', '0', '0'}, []byte{'1', '0', '1', '1', '1'}, []byte{'1', '1', '1', '1', '1'}, []byte{'1', '0', '0', '1', '0'}}
	//data := [][]byte{[]byte{'0', '0', '0'}, []byte{'0', '1', '1'}, []byte{'0', '1', '1'}}
	//data := [][]byte{[]byte{'0', '0', '0'}, []byte{'0', '0', '1'}, []byte{'0', '1', '1'}}
	//data := [][]byte{[]byte{'1', '1', '1'}, []byte{'1', '1', '1'}, []byte{'1', '1', '1'}}
	data := [][]byte{[]byte{'0', '0', '0'}, []byte{'0', '0', '0'}, []byte{'0', '0', '0'}}
	fmt.Println(maximalSquare(data))
}


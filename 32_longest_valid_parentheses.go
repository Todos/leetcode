package main

// Given a string containing just the characters '(' and ')', find the length of the longest valid (well-formed) parentheses substring.
//
// Example 1:
// Input: "(()"
// Output: 2
// Explanation: The longest valid parentheses substring is "()"
//
// Example 2:
// Input: ")()())"
// Output: 4
// Explanation: The longest valid parentheses substring is "()()"

import (
	"sort"
	"strings"
)

type element struct {
	data interface{}
	next *element
}

type stack struct {
	head *element
	Size int
}

func (this *stack) Push(data interface{}) {
	element := new(element)
	element.data = data
	temp := this.head
	element.next = temp
	this.head = element
	this.Size++
}

func (this *stack) Pop() interface{} {
	if this.head == nil {
		return nil
	}
	r := this.head.data
	this.head = this.head.next
	this.Size--

	return r
}

func Constructor() *stack {
	this := new(stack)
	return this
}

var brackets = map[string]string{
	"}": "{",
	"]": "[",
	")": "(",
}

func isOpeningBracket(element string) bool {
	return strings.Compare(element, "[") == 0 || strings.Compare(element, "{") == 0 || strings.Compare(element, "(") == 0
}

func isClosingBracket(element string) bool {
	return strings.Compare(element, "]") == 0 || strings.Compare(element, "}") == 0 || strings.Compare(element, ")") == 0
}

func longestValidParentheses(s string) int {
	var max int = 0
	var elements []int = validParentheses(s)

	for _, element := range elements {
		if max < element {
			max = element
		}
	}
	return max
}

type LeftOversData struct {
	index int
	data  string
}

func validParentheses(s string) (result []int) {
	stack := Constructor()
	var isClosingSequence bool = false // sequence state
	var leftovers []LeftOversData = []LeftOversData{}

	for index, element := range s {
		element := string(element)
		if isOpeningBracket(element) {
			isClosingSequence = false

			var data LeftOversData = LeftOversData{index, element}
			stack.Push(data)
		} else if isClosingBracket(element) {
			value := stack.Pop()
			if value != nil {
				var data LeftOversData = value.(LeftOversData)
				if data.data == brackets[element] {
					isClosingSequence = true
				}
			} else {
				isClosingSequence = false
			}
			if !isClosingSequence {
				var data LeftOversData = LeftOversData{index, element}
				leftovers = append(leftovers, data)
			}
		}
	}

	value := stack.Pop()
	for {
		if value == nil {
			break
		}

		var data LeftOversData = value.(LeftOversData)
		leftovers = append(leftovers, data)

		value = stack.Pop()
	}

	sort.Slice(leftovers, func(i, j int) bool {
		return leftovers[i].index < leftovers[j].index
	})

	var prevIndex int = -1
	for idx, data := range leftovers {
		if idx == 0 {
			if data.index > 0 {
				diff := data.index - prevIndex - 1
				if diff > 0 {
					result = append(result, diff)
				}
			}
			prevIndex = data.index
			continue
		}
		diff := data.index - prevIndex - 1
		if diff > 0 {
			result = append(result, diff)
		}
		prevIndex = data.index
	}
	if prevIndex < len(s)-1 {
		result = append(result, len(s)-prevIndex-1)
	}
	if len(result) == 0 {
		if prevIndex < 0 {
			result = append(result, len(s))
		} else if prevIndex >= 0 {
			result = append(result, len(s)-prevIndex-1)
		}
	}

	return
}

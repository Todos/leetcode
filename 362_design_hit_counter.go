package main

import "fmt"

type HitCounter struct {
	buckets map[int][]int
}


/** Initialize your data structure here. */
func Constructor() HitCounter {
	return HitCounter{
		make(map[int][]int, 0),
	}
}


/** Record a hit.
  @param timestamp - The current timestamp (in seconds granularity). */
func (this *HitCounter) Hit(timestamp int)  {
	reqBucket := this.getBucket(timestamp)
	_, ok := this.buckets[reqBucket]
	if ok == true {
		this.buckets[reqBucket] = append(this.buckets[reqBucket], timestamp)
	} else {
		this.buckets[reqBucket] = []int{timestamp}
	}
}


/** Return the number of hits in the past 5 minutes.
  @param timestamp - The current timestamp (in seconds granularity). */
func (this *HitCounter) GetHits(timestamp int) int {
	bucket := this.getBucket(timestamp)
	prevBucket := -1
	for b, _ := range this.buckets {
		if b > prevBucket && b != bucket {
			prevBucket = b
		}
	}

	hits := []int{}
	for _, t := range []int{bucket, prevBucket} {
		if t < 0 {
			continue
		}
		hits = append(hits, this.buckets[t]...)
	}

	var result int
	for _, h := range hits {
		if h <= timestamp && h > timestamp - 300 {
			result += 1
		}
	}
	return result
}


func (this *HitCounter) getBucket(timestamp int) int {
	return timestamp / 300
}


/**
 * Your HitCounter object will be instantiated and called as such:
 * obj := Constructor();
 * obj.Hit(timestamp);
 * param_2 := obj.GetHits(timestamp);
 */

func main() {
	hc := Constructor()
	hc.Hit(1)
	hc.Hit(2)
	hc.Hit(3)
	res := hc.GetHits(4)
	if res != 3 {
		fmt.Printf("should be 3, but %d\n", res)
	}
	hc.Hit(300)
	res = hc.GetHits(300)
	if res != 4 {
		fmt.Printf("should be 4, but %d\n", res)
	}
	res = hc.GetHits(301)
	if res != 3 {
		fmt.Printf("should be 3, but %d\n", res)
	}
}
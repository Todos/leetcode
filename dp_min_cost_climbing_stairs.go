package main

// You are given an integer array cost where cost[i] is the cost of ith step on a staircase.
// Once you pay the cost, you can either climb one or two steps.
//
// You can either start from the step with index 0, or the step with index 1.
//
// Return the minimum cost to reach the top of the floor.
//
//
// Example 1:
//
// Input: cost = [10,15,20]
// Output: 15
// Explanation: You will start at index 1.
// - Pay 15 and climb two steps to reach the top.
// The total cost is 15.

// Example 2:
//
// Input: cost = [1,100,1,1,1,100,1,1,100,1]
// Output: 6
// Explanation: You will start at index 0.
// - Pay 1 and climb two steps to reach index 2.
// - Pay 1 and climb two steps to reach index 4.
// - Pay 1 and climb two steps to reach index 6.
// - Pay 1 and climb one step to reach index 7.
// - Pay 1 and climb two steps to reach index 9.
// - Pay 1 and climb one step to reach the top.
// The total cost is 6.
//
//
// Constraints:
//
// 2 <= cost.length <= 1000
// 0 <= cost[i] <= 999

// Hint:
// Say f[i] is the final cost to climb to the top from step i. Then f[i] = cost[i] + min(f[i+1], f[i+2]).

func minCostClimbingStairs(cost []int) int {

	var memo = make(map[int]int)
	return minCost(len(cost), cost, memo)
}

func minCost(i int, cost []int, memo map[int]int) int {
	if i <= 1 {
		return 0
	}

	if val, ok := memo[i]; ok {
		return val
	}

	var next1 = cost[i-1] + minCost(i-1, cost, memo)
	var next2 = cost[i-2] + minCost(i-2, cost, memo)

	if next1 > next2 {
		memo[i] = next2
	} else {
		memo[i] = next1
	}

	return memo[i]
}

package main

import (
    "reflect"
    "testing"
)

var tableData = []struct {
    workers [][]int
    bikes [][]int
    result []int
} {
    {[][]int{[]int{0, 0}, []int{2, 1}},
        [][]int{[]int{1, 2}, []int{3, 3}},
        []int{1, 0}},
    {[][]int{[]int{0, 0}, []int{1, 1}, []int{2, 0}},
        [][]int{[]int{1, 0}, []int{2, 2}, []int{2, 1}},
        []int{0, 2, 1}},
}

func TestAssign1(t *testing.T) {
    for _, d := range tableData {
        res := assignBikes(d.workers, d.bikes)
        if !reflect.DeepEqual(res, d.result) {
            t.Error("For", d.workers,
                "workers and", d.bikes, "bikes",
                "got", res, "but should", d.result)
        }
    }
}

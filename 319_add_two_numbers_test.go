package main

import (
    "reflect"
    "testing"
)

func getSlice(l *ListNode) []int {
    if l == nil {
        return []int{}
    }

    result := []int{}
    for {
        result = append(result, l.Val)
        if l.Next == nil {
            break
        }

        l = l.Next
    }
    return result
}

func getList(s []int) *ListNode {
    if len(s) == 0 {
        return nil
    }

    head := &ListNode{Val: s[0], Next: nil}
    tail := head
    for _, e := range s[1:] {
        ln := ListNode{Val: e}
        tail.Next = &ln
        tail = &ln
    }
    return head
}

var dat1 = []struct {
    in1 []int
    in2 []int
    out []int
} {
    {in1: []int{2,4,3}, in2: []int{5,6,4}, out: []int{7,0,8}},
    {in1: []int{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1}, in2: []int{5,6,4}, out: []int{6,6,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1}},
    {in1: []int{0}, in2: []int{0}, out: []int{0}},
    {in1: []int{5}, in2: []int{5}, out: []int{0,1}},
}

func Test1(t *testing.T) {
    for _, r := range dat1 {
        l1 := getList(r.in1)
        l2 := getList(r.in2)
        res := addTwoNumbers(l1, l2)
        s1 := getSlice(res)
        if !reflect.DeepEqual(s1, r.out) {
            t.Error("Got", s1, "expected", r.out)
        }
    }
}

package main

import "testing"

var data = []struct{
	nums []int
	out int
} {
	{nums: []int{3, 0, 1}, out: 2},
	{nums: []int{0, 1}, out: 2},
	{nums: []int{9,6,4,2,3,5,7,0,1}, out: 8},
	{nums: []int{0}, out: 1},
}

func TestMissingNumber(t *testing.T) {
	for _, d := range data {
		res := missingNumber(d.nums)
		if res != d.out {
			t.Error("nums:", d.nums, "got:", res, "expected:", d.out)
		}
	}

}

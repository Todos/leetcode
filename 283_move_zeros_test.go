package main

import (
   "reflect"
   "testing"
)

var dataTable = []struct{
   in []int
   out []int
} {
   {in: []int{1, 0}, out: []int{1, 0}},
   {in: []int{0, 1}, out: []int{1, 0}},
   {in: []int{10, 0, 5, 0, 1}, out: []int{10, 5, 1, 0, 0}},
   {in: []int{0, 1, 0, 3, 12}, out: []int{1, 3, 12, 0, 0}},
}

func Test1(t *testing.T) {
   for _, x := range dataTable {
      moveZeroes(x.in)
      if !reflect.DeepEqual(x.in, x.out) {
         t.Error("For", x.in, "expected", x.out)
      }
   }
}
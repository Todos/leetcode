package main

import "math"

var (
	minValue = -int(math.Pow10(7))
)

type MaxStack struct {
	elements []int
	max int
}



/** initialize your data structure here. */
func MaxStackConstructor() MaxStack {
	return MaxStack{[]int{}, minValue}
}


func (this *MaxStack) Push(x int)  {
	this.elements = append(this.elements, x)
	if x > this.max {
		this.max = x
	}
}


func (this *MaxStack) Pop() int {
	lenE := len(this.elements)
	element := this.elements[lenE-1]
	this.elements = this.elements[:lenE-1]
	if element == this.max {
		this.updateMax()
	}

	return element
}


func (this *MaxStack) Top() int {
	lenE := len(this.elements)
	return this.elements[lenE-1]
}


func (this *MaxStack) PeekMax() int {
	return this.max
}


func (this *MaxStack) PopMax() int {
	var idx int
	var element int
	for i := len(this.elements)-1; i > 0; i-- {
		if this.elements[i] == this.max {
			idx = i
			break
		}
	}

	element = this.elements[idx]
	this.elements = append(this.elements[:idx], this.elements[idx+1:]...)
	this.updateMax()
	return element
}


func (this *MaxStack) updateMax() {
	this.max = minValue
	lenE := len(this.elements)
	for i := 0; i < lenE; i++ {
		if this.elements[i] > this.max {
			this.max = this.elements[i]
		}
	}
}


/**
 * Your MaxStack object will be instantiated and called as such:
 * obj := Constructor();
 * obj.Push(x);
 * param_2 := obj.Pop();
 * param_3 := obj.Top();
 * param_4 := obj.PeekMax();
 * param_5 := obj.PopMax();
 */

func main() {
	stk := MaxStackConstructor()
	stk.Push(5)   // [5] the top of the stack and the maximum number is 5.
	stk.Push(1)   // [5, 1] the top of the stack is 1, but the maximum is 5.
	stk.Push(5)   // [5, 1, 5] the top of the stack is 5, which is also the maximum, because it is the top most one.
	stk.Top()     // return 5, [5, 1, 5] the stack did not change.
	stk.PopMax()  // return 5, [5, 1] the stack is changed now, and the top is different from the max.
	stk.Top()     // return 1, [5, 1] the stack did not change.
	stk.PeekMax() // return 5, [5, 1] the stack did not change.
	stk.Pop()     // return 1, [5] the top of the stack and the max element is now 5.
	stk.Top()     // return 5, [5] the stack did not change.
}
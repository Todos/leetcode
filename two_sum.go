package main

/*
Given an array of integers, return indices of the two numbers such that they add up to a specific target.

You may assume that each input would have exactly one solution, and you may not use the same element twice.

Example:

Given nums = [2, 7, 11, 15], target = 9,

Because nums[0] + nums[1] = 2 + 7 = 9,
return [0, 1].
 */

func twoSum(nums []int, target int) []int {
    h1 := make(map[int][]int, 0)
    for i, n := range nums {
        _, ok := h1[n]
        if !ok {
            h1[n] = []int{i}
        } else {
            h1[n] = append(h1[n], i)
        }
    }

    var res []int
    for i, k := range nums {
        v := h1[k]
        n := target - k
        if n == k {
            if len(v) > 1 {
                res = v[0:2]
            } else {
                //res = []int{v[0], v[0]}
                continue
            }
            break
        }

        e, ok := h1[n]
        if ok {
            res = []int{i, e[0]}
            break
        }
    }
    return res
}


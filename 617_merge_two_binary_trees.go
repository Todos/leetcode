package main

import "fmt"

/*
Given two binary trees and imagine that when you put one of them to cover the other, some nodes of the two trees are overlapped while the others are not.

You need to merge them into a new binary tree. The merge rule is that if two nodes overlap, then sum node values up as the new value of the merged node. Otherwise, the NOT null node will be used as the node of new tree.

Example 1:

Input:
	Tree 1                     Tree 2
          1                         2
         / \                       / \
        3   2                     1   3
       /                           \   \
      5                             4   7
Output:
Merged tree:
	     3
	    / \
	   4   5
	  / \   \
	 5   4   7


Note: The merging process must start from the root nodes of both trees.
 */

type TreeNode struct {
    Val int
    Left *TreeNode
    Right *TreeNode
}

func mergeTrees(t1 *TreeNode, t2 *TreeNode) *TreeNode {
	if t1 != nil && t2 != nil {
		t2.Val += t1.Val
	}
	if t2 == nil {
		return t1
	}
	traverseMergeBST(t1, t2)
	return t2
}

func traverseMergeBST(t1 *TreeNode, t2 *TreeNode) {
	if t1 == nil {
		return
	}

	if t2 == nil {
		t2 = &TreeNode{t1.Val, nil, nil}
	}

	if t1.Left != nil && t2.Left == nil {
		t2.Left = &TreeNode{t1.Left.Val, nil, nil}
	} else if t1.Left != nil && t2.Left != nil {
		t2.Left.Val += t1.Left.Val
	}

	if t1.Right != nil && t2.Right == nil {
		t2.Right = &TreeNode{t1.Right.Val, nil, nil}
	} else if t1.Right != nil && t2.Right != nil {
		t2.Right.Val += t1.Right.Val
	}

	traverseMergeBST(t1.Left, t2.Left)
	traverseMergeBST(t1.Right, t2.Right)
}


func buildBST(list []int) *TreeNode {
	var node *TreeNode = nil
	for _, x := range list {
		node = insertNode(node, x)
	}
	return node
}

func insertNode(root *TreeNode, val int) *TreeNode {
	if root == nil {
		root = &TreeNode{val, nil, nil}
	} else if val == root.Val {
		root.Val = val
	} else if val < root.Val {
		// left side we're at
		root.Left = insertNode(root.Left, val)
	} else if val > root.Val {
		// right side we're at
		root.Right = insertNode(root.Right, val)
	}
	return root
}

func main() {
	var t1 = buildBST([]int{1,2})
	var t2 = buildBST([]int{20,10})

	var t3 = mergeTrees(t1, t2)
	fmt.Println(t3)

	t1 = &TreeNode{1, &TreeNode{2, nil, nil}, nil}
	t2 = &TreeNode{1, nil, &TreeNode{3, nil, nil}}
	t3 = mergeTrees(t1, t2)
	fmt.Println(t3)

	t1 = &TreeNode{1, nil, nil}
	t2 = nil
	t3 = mergeTrees(t1, t2)
	fmt.Println(t3)
}

package main

import (
	"testing"
)

var data = []struct{
	d int
	res int
}{
	{123, 321},
	{-123, -321},
	{120, 21},
	{0, 0},
	{901000, 109},
	{1534236469, 0},

}

func Test1(t *testing.T) {
	for _, x := range data {
		res := reverse2(x.d)
		if res != x.res {
			t.Error("For", x.d, "expected", x.res, "got", res)
		}
	}
}

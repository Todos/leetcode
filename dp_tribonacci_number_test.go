package main

import "testing"

var data = []struct {
	in  int
	out int
}{
	{4, 4},
	{25, 1389537},
}

func Test1(t *testing.T) {
	for _, d := range data {
		result := tribonacci(d.in)
		if result != d.out {
			t.Error("For", d.in, "expected", d.out, "got", result)
		}
	}
}

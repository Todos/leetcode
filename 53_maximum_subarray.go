package main

import "fmt"

/*
Given an integer array nums, find the contiguous subarray (containing at least one number) which has the largest sum
and return its sum.

Example 1:

Input: nums = [-2,1,-3,4,-1,2,1,-5,4]
Output: 6
Explanation: [4,-1,2,1] has the largest sum = 6.

Example 2:

Input: nums = [1]
Output: 1

Example 3:

Input: nums = [5,4,-1,7,8]
Output: 23
 */

func maxSubArray(nums []int) int {
	if len(nums) == 0 {
		return 0
	}

	var result = -1<<31

	for i := 0; i < len(nums); i++ {
		subMax := 0
		for j := i; j < len(nums); j++ {
			subMax += nums[j]
			if subMax > result {
				result = subMax
			}
		}
	}

	return result
}

func main() {
	fmt.Println(maxSubArray([]int{1, 2, 3}))
	fmt.Println(maxSubArray([]int{-1, -2, -3}))
	fmt.Println(maxSubArray([]int{}))
}
package main

import (
    "testing"
)

var data = []struct {
    words []string
    word1 string
    word2 string
    out int
}{
    {[]string{"practice", "makes", "perfect", "coding", "makes"}, "coding", "practice", 3},
    {[]string{"practice", "makes", "perfect", "coding", "makes"}, "makes", "coding", 1},
}

func Test1(t *testing.T) {
    for _, d := range data {
        res := shortestDistance(d.words, d.word1, d.word2)
        if res != d.out {
            t.Error("For", d.words, "and", d.word1, "and", d.word2, "got", res, "expected", d.out)
        }
    }
}

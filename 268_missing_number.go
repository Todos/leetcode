package main

import "sort"

func missingNumber(nums []int) int {
	numsLen := len(nums)
	sort.Ints(nums)

	result := -1
	for i, val := range nums {
		if i != val {
			return i
		}
	}

	if result == -1 {
		// result will be the last element in the slice...
		result = numsLen
	}
	return result
}


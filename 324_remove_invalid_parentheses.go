package main

import (
    "fmt"
    "strings"
)

/*
Remove the minimum number of invalid parentheses in order to make the input string valid. Return all possible results.

Note: The input string may contain letters other than the parentheses ( and ).

Example 1:

Input: "()())()"
Output: ["()()()", "(())()"]

Example 2:

Input: "(a)())()"
Output: ["(a)()()", "(a())()"]

Example 3:

Input: ")("
Output: [""]
 */


func removeInvalidParentheses(s string) []string {
    opening, closing := getMisplaced(s)
    combs := combinations(s, len(s), len(opening) + len(closing))
    perms := permutations(s, combs)

    res := make([]string, 0)
    for _, p := range perms {
        if isValid(p) {
            res = append(res, p)
        }
    }

    return res
}

func getMisplaced(s string) (opening []int, closing []int) {
    type elementIndexed struct {
        val string
        idx int
    }

    stack := NewStack()
    for i, element := range s {
        element := string(element)
        if isOpeningBracket(element) {
            e := elementIndexed{val: element, idx: i}
            stack.Push(e)
        } else if isClosingBracket(element) {
            value := stack.Pop()
            if value == nil || value.(elementIndexed).val != brackets[element] {
                closing = append(closing, i)
            }
        }
    }

    for stack.Size > 0 {
        value := stack.Pop()
        v := value.(elementIndexed)
        opening = append(opening, v.idx)

    }
    return opening, closing
}

// Function that finds all combinations of size r
// in arr[] of size n. This function mainly uses combinationsHelper()
func combinations(s string, n int, r int) [][]Dat {
    // A temporary array to store all combination one by one
    data := make([]Dat, r)

    arr := strings.Split(s, "")
    res := make([][]Dat, 0)

    // Get all combination using temporary array 'data[]'
    combinationsHelper(arr, n, r, 0, data, 0, &res)
    return res
}

func permutations(s string, cs [][]Dat) []string {
    ss := strings.Split(s, "")
    result := make([]string, 0)
    res := make([][]string, 0)

    cs2 := make([]map[int]string, 0)
    for i := 0; i < len(cs); i++ {
        cs3 := make(map[int]string, 0)
        for j := 0; j < len(cs[i]); j++ {
            cs3[cs[i][j].idx] = cs[i][j].elt
        }
        cs2 = append(cs2, cs3)
    }

    for i := 0; i < len(cs2); i++ {
        ss2 := make([]string, 0)

        for j := 0; j < len(ss); j++ {
            e, ok := cs2[i][j]
            if ok && (isOpeningBracket(e) || isClosingBracket(e)) {
                continue
            }

            ss2 = append(ss2, ss[j])
        }

        res = append(res, ss2)
    }

    for i := 0; i < len(res); i++ {
        r := strings.Join(res[i], "")
        if !elementIn(result, r) {
            result = append(result, r)
        }
    }

    return result
}

/*
https://www.geeksforgeeks.org/print-all-possible-combinations-of-r-elements-in-a-given-array-of-size-n/
Method 2
 */

type Dat struct {
    elt string
    idx int
}

/* arr[]  ---> Input Array
   n      ---> Size of input array
   r      ---> Size of a combination to be printed
   index  ---> Current index in data[]
   data[] ---> Temporary array to store current combination
   i      ---> index of current element in arr[]     */
func combinationsHelper(arr []string, n int, r int, index int, data []Dat, i int, result *[][]Dat) {
    // Current combination is ready, print it
    if index == r {
        d := make([]Dat, len(data))
        copy(d, data)
        *result = append(*result, d)
        return
    }

    // When no more elements are there to put in data[]
    if i >= n {
        return
    }

    // current is included, put next at next location
    data[index] = Dat{elt: arr[i], idx: i}
    combinationsHelper(arr, n, r, index+1, data, i+1, result)

    // current is excluded, replace it with next (Note that
    // i+1 is passed, but index is not changed)
    combinationsHelper(arr, n, r, index, data, i+1, result)
}

func elementIn(ss []string, s string) bool {
    for _, e := range ss {
        if e == s {
            return true
        }
    }
    return false
}

type element struct {
    data interface{}
    next *element
}

type stack struct {
    head *element
    Size int
}

func (s *stack) Push(data interface{}) {
    element := new(element)
    element.data = data
    temp := s.head
    element.next = temp
    s.head = element
    s.Size++
}

func (s *stack) Pop() interface{} {
    if s.head == nil {
        return nil
    }
    r := s.head.data
    s.head = s.head.next
    s.Size--

    return r
}

func NewStack() *stack {
    this := new(stack)
    return this
}

var brackets = map[string]string{
    ")": "(",
}

func isOpeningBracket(element string) bool {
    return strings.Compare(element, "(") == 0
}

func isClosingBracket(element string) bool {
    return strings.Compare(element, ")") == 0
}

func isValid(s string) bool {
    stack := NewStack()
    for _, element := range s {
        element := string(element)
        if isOpeningBracket(element) {
            stack.Push(element)
        } else if isClosingBracket(element) {
            value := stack.Pop()
            if value != brackets[element] {
                return false
            }
        }
    }
    if stack.Size > 0 {
        return false
    }
    return true
}

func main() {

    //fmt.Println(combinations("abcdefghi", 9, 4))
    pat := "(((k()(("
    op, cl := getMisplaced(pat)
    combs := combinations(pat, len(pat), len(op) + len(cl))
    fmt.Println(combs)
    fmt.Println(permutations(pat, combs))

    // For debug
    validPatterns := []string{
        "()()()",
        "(())()",
        "(a)()()",
        "(a())()",
        "",
        "((",
        "(()",
    }

    for _, p := range validPatterns {
        if !isValid(p) {
            fmt.Printf("Error: pattern \"%s\" expected to be valid\n", p)
        }
    }

    fmt.Println(removeInvalidParentheses("(((k()(("))
    fmt.Println(removeInvalidParentheses("))"))
    fmt.Println(removeInvalidParentheses("n"))
    fmt.Println(removeInvalidParentheses(")("))
    fmt.Println(removeInvalidParentheses("()()()"))
    fmt.Println(removeInvalidParentheses("(a)(b)(c)"))
    fmt.Println(removeInvalidParentheses("()())()"))
    fmt.Println(removeInvalidParentheses("(a)())()"))

    fmt.Println("---")
    s := "()))((()"
    op, cl = getMisplaced(s)
    fmt.Printf("%s: %v %v\n", s, op, cl)

    s = ")("
    op, cl = getMisplaced(s)
    fmt.Printf("%s: %v %v\n", s, op, cl)

    s = "n"
    op, cl = getMisplaced(s)
    fmt.Printf("%s: %v %v\n", s, op, cl)

    s = "()())()"
    op, cl = getMisplaced(s)
    fmt.Printf("%s: %v %v\n", s, op, cl)

    s = "()())"
    op, cl = getMisplaced(s)
    fmt.Printf("%s: %v %v\n", s, op, cl)

    s = "(a)())()"
    op, cl = getMisplaced(s)
    fmt.Printf("%s: %v %v\n", s, op, cl)
}

package main

import (
	"fmt"
	"strconv"
	"strings"
)

/*
Given a signed 32-bit integer x, return x with its digits reversed. If reversing x causes the value to go outside the
signed 32-bit integer range [-2^31, 2^31 - 1], then return 0.
Assume the environment does not allow you to store 64-bit integers (signed or unsigned).
 */

func reverse2(x int) int {
	if x < -1<<31 || x > 1<<31 - 1 {
		return 0
	}

	s1 := fmt.Sprintf("%d", x)
	var elems1 = strings.Split(s1, "")
	var elems2 = make([]string, 0)
	isNeg := false

	for i, e := range elems1 {
		if i == 0 && e == "-" {
			isNeg = true
			continue
		}
		elems2 = append([]string{e}, elems2...)
	}

	z := 0
	for i, e := range elems2 {
		if e == "0" {
			z = i
		} else {
			break
		}
	}

	elems2 = elems2[z:]

	s2 :=strings.Join(elems2, "")
	res, _ := strconv.Atoi(s2)

	if res > 1<<31 - 1 {
		return 0
	}

	if isNeg {
		return -res
	}
	return res
}

package main

import "testing"

func TestRandomizedSet1(t *testing.T) {
	rs := RSConstructor()
	res1 := rs.Insert(1)	// Inserts 1 to the set. Returns true as 1 was inserted successfully.
	if !res1 {
		t.Fail()
	}
	res2 := rs.Remove(2)	// Returns false as 2 does not exist in the set.
	if res2 {
		t.Fail()
	}
	res3 := rs.Insert(2)	// Inserts 2 to the set, returns true. Set now contains [1,2].
	if !res3 {
		t.Fail()
	}
	res4 := rs.GetRandom()		// getRandom() should return either 1 or 2 randomly.
	if res4 != 1 && res4 != 2 {
		t.Fail()
	}
	res5 := rs.Remove(1)	// Removes 1 from the set, returns true. Set now contains [2].
	if !res5 {
		t.Fail()
	}
	res6 := rs.Insert(2)	// 2 was already in the set, so return false.
	if res6 {
		t.Fail()
	}
	res7 := rs.GetRandom()		// Since 2 is the only number in the set, getRandom() will always return 2.
	if res7 != 2 {
		t.Fail()
	}

}

"""
# Definition for a Node.
class Node(object):
    def __init__(self, val, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
"""


class Node(object):
    def __init__(self, val, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

    def __str__(self):
        return "[{} <= {}:{} => {}]".format(id(self.left), id(self), self.val, id(self.right))


class Solution(object):
    def insertNode(self, root, val):
        if not root:
            root = Node(val, None, None)
        elif val < root.val:
            root.left = self.insertNode(root.left, val)
        elif val > root.val:
            root.right = self.insertNode(root.right, val)
        return root

    def buildBST(self, node_list):
        node = None
        for x in node_list:
            node = self.insertNode(node, x)
        return node

    def getList(self, head):
        if head is None:
            return []

        hval = head.val
        res = [hval]
        node = head.right
        while True:
            if node is None:
                break
            if node.val == hval:
                break

            res.append(node.val)
            node = node.right

        return res

    def traverseBST(self, node, nodeList):
        if not node:
            return nodeList

        nodeListLeft = self.traverseBST(node.left, nodeList)
        nodeList = Node(node.val, nodeListLeft, None)

        return self.traverseBST(node.right, nodeList)

    def treeToDoublyList(self, root):
        """
        :type root: Node
        :rtype: Node
        """
        tail = self.traverseBST(root, None)
        head = None
        node = tail

        while node:
            if node.left is None:
                head = node
                break
            else:
                nodeLeft = node.left
                nodeLeft.right = node
                node = nodeLeft

        if head:
            head.left = tail
        if tail:
            tail.right = head
        return head


if __name__ == '__main__':

    def check(inp, exp):
        s1 = Solution()

        bst1 = s1.buildBST(inp)
        h1 = s1.treeToDoublyList(bst1)
        l1 = s1.getList(h1)
        assert l1 == exp

    check([2, 1, 3], [1, 2, 3])
    check([4, 2, 5, 1, 3], [1, 2, 3, 4, 5])
    check([], [])
    check([1], [1])

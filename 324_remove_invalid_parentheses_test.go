package main

import (
    "fmt"
    "reflect"
    "sort"
    "testing"
)

var data = []struct{
    in string
    out []string
} {
    {in: "(((k()((", out: []string{"k()", "(k)"}},
    {in: "()())()", out: []string{"()()()", "(())()"}},
    {in: "(a)())()", out: []string{"(a)()()", "(a())()"}},
    {in: "(a)(()()", out: []string{"(a)()()", "(a)(())"}},
    {in: ")(", out: []string{""}},
    {in: "n", out: []string{"n"}},
    {in: "))", out: []string{""}},
}

func Test1(t *testing.T) {
    for _, d := range data {
        fmt.Printf("test: %#v\n", d)
        result := removeInvalidParentheses(d.in)
        sort.Strings(result)
        sort.Strings(d.out)
        if !reflect.DeepEqual(result, d.out) {
            t.Errorf("For %#v expected %#v got %#v", d.in, d.out, result)
        }
    }
}

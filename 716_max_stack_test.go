package main

import "testing"

func TestMaxStack(t *testing.T) {
	stk := MaxStackConstructor()
	stk.Push(5)   // [5] the top of the stack and the maximum number is 5.
	stk.Push(1)   // [5, 1] the top of the stack is 1, but the maximum is 5.
	stk.Push(5)   // [5, 1, 5] the top of the stack is 5, which is also the maximum, because it is the top most one.
	res := stk.Top()     // return 5, [5, 1, 5] the stack did not change.
	if res != 5 {
		t.Fail()
	}

	res = stk.PopMax()  // return 5, [5, 1] the stack is changed now, and the top is different from the max.
	if res != 5 {
		t.Fail()
	}

	res = stk.Top()     // return 1, [5, 1] the stack did not change.
	if res != 1 {
		t.Fail()
	}

	res = stk.PeekMax() // return 5, [5, 1] the stack did not change.
	if res != 5 {
		t.Fail()
	}

	res = stk.Pop()     // return 1, [5] the top of the stack and the max element is now 5.
	if res != 1 {
		t.Fail()
	}

	res = stk.Top()     // return 5, [5] the stack did not change.
	if res != 5 {
		t.Fail()
	}
}
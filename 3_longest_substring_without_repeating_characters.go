package main

/*
Given a string s, find the length of the longest substring without repeating characters.

Example 1:

Input: s = "abcabcbb"
Output: 3
Explanation: The answer is "abc", with the length of 3.

Example 2:

Input: s = "bbbbb"
Output: 1
Explanation: The answer is "b", with the length of 1.

Example 3:

Input: s = "pwwkew"
Output: 3
Explanation: The answer is "wke", with the length of 3.
Notice that the answer must be a substring, "pwke" is a subsequence and not a substring.

Example 4:

Input: s = ""
Output: 0

Constraints:

0 <= s.length <= 5 * 104
s consists of English letters, digits, symbols and spaces.
 */

func lengthOfLongestSubstring(s string) int {
	maxLen := 0
	for i := 0; i < len(s); i++ {
		s2 := s[i:]
		l2 := make([]uint8, 0)
		for k := 0; k < len(s2); k++ {
			if elementContained(s2[k], l2){
				break
			} else {
				l2 = append(l2, s2[k])
				if len(l2) > maxLen {
					maxLen = len(l2)
				}
			}
		}
	}
	return maxLen
}

func elementContained(e uint8, s []uint8) bool {
	for _, elt := range s {
		if e == elt {
			return true
		}
	}
	return false
}
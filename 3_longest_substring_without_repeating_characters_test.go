package main

import "testing"

var data = []struct{
	input string
	output int
} {
	{"abcabcbb", 3},
	{"bbbbb", 1},
	{"pwwkew", 3},
	{"", 0},
	{" ", 1},
}

func Test1(t *testing.T) {
	for _, d := range data {
		res := lengthOfLongestSubstring(d.input)
		if d.output != res {
			t.Error("For", d.input, "got", res, "expected", d.output)
		}
	}
}
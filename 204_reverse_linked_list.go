package main

import "fmt"

/*
Reverse a singly linked list.

Example:

Input: 1->2->3->4->5->NULL
Output: 5->4->3->2->1->NULL
Follow up:

A linked list can be reversed either iteratively or recursively. Could you implement both?
*/

type ListNode struct {
	Val  int
	Next *ListNode
}

/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func reverseList(head *ListNode) *ListNode {
	//return reverseListIterative(head)
	return reverseListRecursive(head)
}

func reverseListRecursive(head *ListNode) *ListNode {
	if head == nil || head.Next == nil {
		return head
	}

	var tmp = reverseListRecursive(head.Next)
	head.Next.Next = head
	head.Next = nil
	return tmp
}

func reverseListIterative(head *ListNode) *ListNode {
	if head == nil {
		return nil
	}

	if head.Next == nil {
		return head
	}

	var (
		prev *ListNode = nil
		curr *ListNode = nil
	)

	curr = head

	for {
		if curr == nil {
			break
		}

		var temp *ListNode = curr.Next
		curr.Next = prev
		prev = curr
		curr = temp
	}

	return prev
}

func getSlice(l *ListNode) []int {
	if l == nil {
		return []int{}
	}

	result := []int{}
	for {
		result = append(result, l.Val)
		if l.Next == nil {
			break
		}

		l = l.Next
	}
	return result
}

func getList(s []int) *ListNode {
	if len(s) == 0 {
		return nil
	}

	head := &ListNode{Val: s[0], Next: nil}
	tail := head
	for _, e := range s[1:] {
		ln := ListNode{Val: e}
		tail.Next = &ln
		tail = &ln
	}
	return head
}

func main() {
	l1 := []int{1, 2, 3, 4, 5}
	rl1 := reverseList(getList(l1))
	l2 := getSlice(rl1)
	fmt.Println(l1, l2)
}

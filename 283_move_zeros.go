package main

/*
Given an array nums, write a function to move all 0's to the end of it while maintaining the relative order of the non-zero elements.

Example:

Input: [0,1,0,3,12]
Output: [1,3,12,0,0]

Note:

    You must do this in-place without making a copy of the array.
    Minimize the total number of operations.
 */

/*
Used this trick: https://github.com/golang/go/wiki/SliceTricks#filtering-without-allocating
This trick uses the fact that a slice shares the same backing array and capacity as the original,
so the storage is reused for the filtered slice. Of course, the original contents are modified.

b := a[:0]
for _, x := range a {
	if f(x) {
		b = append(b, x)
	}
}
 */

func moveZeroesFirstAcceptedSubOptimal(nums []int)  {
    res := nums[:0]
    zeroCount := 0
    for _, x := range nums {
        if x != 0 {
            res = append(res, x)
        } else {
            zeroCount++
        }
    }

    for i := 0; i < zeroCount; i ++ {
        res = append(res, 0)
    }

    return
}

func moveZeroes(nums []int)  {
    lastNonZeroSeen := 0
    for i, x := range nums {
        if x != 0 {
            nums[lastNonZeroSeen] = nums[i]
            lastNonZeroSeen += 1
        }
    }

    for i := lastNonZeroSeen; i < len(nums); i ++ {
        nums[i] = 0
    }

    return
}

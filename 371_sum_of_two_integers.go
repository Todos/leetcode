package main

// Calculate the sum of two integers a and b, but you are not allowed to use the operator + and -.
//
// Example 1:
//
// Input: a = 1, b = 2
// Output: 3
//
// Example 2:
//
// Input: a = -2, b = 3
// Output: 1

import "fmt"

func getSum(a int, b int) int {
	sum := a
	carry := b
	for carry != 0 {
		carry2 := sum & carry
		sum = sum ^ carry
		carry = carry2 << 1
	}
	return sum
}

func main() {
	fmt.Println("2 3", getSum(2, 3))
	fmt.Println("-1 3", getSum(-1, 3))
	fmt.Println("-1 -3", getSum(-1, -3))
}

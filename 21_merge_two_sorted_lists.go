package main

/*
Merge two sorted linked lists and return it as a new list. The new list should be made by splicing together the nodes of the first two lists.

Example:

Input: 1->2->4, 1->3->4
Output: 1->1->2->3->4->4
 */

/**
* Definition for singly-linked list.
* type ListNode struct {
*     Val int
*     Next *ListNode
* }
*/

type ListNode struct {
    Val int
    Next *ListNode
}

func appendTo(head *ListNode, tail *ListNode, node *ListNode) (*ListNode, *ListNode) {
    if head == nil {
        head = node
        tail = node
    } else {
        tail.Next = node
        tail = node
    }
    return head, tail
}

func mergeTwoLists(l1 *ListNode, l2 *ListNode) *ListNode {
    var (
        head *ListNode
        tail *ListNode
    )
    for {
        if l1 == nil && l2 == nil {
            break
        } else if l1 == nil {
            head, tail = appendTo(head, tail, &ListNode{Val: l2.Val})
            l2 = l2.Next
        } else if l2 == nil {
            head, tail = appendTo(head, tail, &ListNode{Val: l1.Val})
            l1 = l1.Next
        } else {
            if l1.Val <= l2.Val {
                head, tail = appendTo(head, tail, &ListNode{Val: l1.Val})
                l1 = l1.Next
            } else {
                head, tail = appendTo(head, tail, &ListNode{Val: l2.Val})
                l2 = l2.Next
            }
        }
    }

    return head
}


package main

// ➜  leetcode git:(master) ✗ go test 20_valid_parentheses.go 20_valid_parentheses_test.go
// --- FAIL: TestValidParentheses (0.00s)
// 	20_valid_parentheses_test.go:24: For (] expected false got true
// 	20_valid_parentheses_test.go:24: For ([)] expected false got true
// FAIL
// FAIL	command-line-arguments	0.010s

import (
	"testing"
)

type testpair struct {
	input  string
	result bool
}

var tests = []testpair{
	{"()", true},
	{"()[]{}", true},
	{"(]", false},
	{"([)]", false},
	{"{[]}", true},
	{"", true},
}

func TestValidParentheses(t *testing.T) {
	for _, pair := range tests {
		res := isValid(pair.input)
		if pair.result != res {
			t.Error(
				"For", pair.input,
				"expected", pair.result,
				"got", res,
			)
		}
	}

}
